*** Settings ***
Variables         case_registration_inputs.py
Library           DateTime
Library           Collections
Variables         case_registration_path.py
Variables         candidate_page.py
Variables         create_password.py
Variables         gmail_path_ram.py
Variables         homepage_path.py
Variables         dataentry_path_new.py
Library           String
Library           Selenium2Library
Variables         vendor_master_path.py
Resource          Keywords.robot
Variables         vendor_inputs.py
Variables         sitepreference_path.py
Variables         fieldsetup_path.py
Variables         caseassignment_path.py
Variables         Verification_Values.py
Variables         YTR_Clear_path.py
Variables         logout_path.py

*** Variables ***
${value_count}    0
@{multiple_component}    ${component_eduselect}    ${component_refselect}    ${component_addselect}
@{multiple_selected_check}    ${vendorcheck1}    ${vendorcheck3}    ${vendorcheck5}    ${vendorcheck6}
@{multiple_deselected_check}    ${vendorcheck2}    ${vendorcheck4}
@{field_enabled}    Gender    Father's First Name    Father's Last Name    Marital Status    Nationality    Candidate ID    Employee ID
...               Landline Number    Mobile Number    Notification Mail
@{field_disabled}    First Name    Last Name    Date of Birth    Email    Select category of candidate    Please select who is supposed to do data entry    Client Approval
@{educationcomponent}    10th    12th    UG1    UG2    Diploma/ITI    Under Graduate 1    Under Graduate 2
...               Highest 1    Highest 2    Highest 3    Highest 4
@{referencecomponent}    Reference 1    Reference 2    Reference 3    Reference 4
@{employmentcomponent}    Employment 1    Employment 2    Employment 3    Employment 4
@{addresscomponent}    Current Address    Permanent Address    Previous Address 1    Previous Address 2
@{criminalcomponent}    Current Address Criminal Check    Permanent Address Criminal Check    Previous Address 1 Criminal Check    Previous Address 2 Criminal Check
@{idcomponent}    Aadhaar Card    Driving Licence    Voters ID    National Insurance Card    National ID Card    PAN Card    Passport
...               Social Insurance Number    Social Security Number    Ration card
@{databasecomponent}    Database
#path - Case Registration
${fatherfname_mandatorystar}    //label[contains(text(),"Father's First Name")]/em
${fatherlname_mandatorystar}    //label[contains(text(),"Father's Last Name")]/em
