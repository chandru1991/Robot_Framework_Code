*** keywords ***
Education_Check Selection
    ${ID_Text}=    set variable    //div[@ng-if="check.checkname=='education'"]/div
    ${ID_Count}=    Get matching xpath count    ${ID_Text}
    Set Global Variable    ${ID_Text}
    Log To Console    ${ID_Count}
    : FOR    ${INDEX}    IN RANGE    1    ${ID_Count}+1
    \    Log To Console    ${INDEX}
    \    Set Global Variable    ${INDEX}
    \    ${ID}=    Get Text    xpath=(${ID_Text})[${INDEX}]
    \    log to console    ${ID}
    \    Set Global Variable    ${ID}
    \    Run Keyword If    '${ID}' == '10th'    Run Keyword    Education_Check BOX
    \    Run Keyword If    '${ID}' == '12th'    Run Keyword    Education_Check BOX
    \    Run Keyword If    '${ID}' == 'Diploma/ITI'    Run Keyword    Education_Check BOX
    \    Run Keyword If    '${ID}' == 'Under Graduate'    Run Keyword    Education_Check BOX
    \    Run Keyword If    '${ID}' == 'Post Graduate'    Run Keyword    Education_Check BOX
    \    Run Keyword If    '${ID}' == 'Professional'    Run Keyword    Education_Check BOX
    \    #Run Keyword If    '${ID}' == 'Highest'    Run Keyword    Education_Check BOX

Education_Check BOX
    ${Check_Box}=    set variable    //div[@ng-if="check.checkname=='education'"]/div/input[1]    #//input[@ng-model='compo.selected']
    ${Count}=    Get matching xpath count    ${Check_Box}
    Set Global Variable    ${Check_Box}
    #Log To Console    ${INDEX}
    #Log To Console    ${Count}
    click element    xpath=(${Check_Box})[${INDEX}]

Address_Check Selection
    ${ID_Text}=    set variable    //div[@ng-if="check.checkname=='address'"]/ul/div
    ${ID_Count}=    Get matching xpath count    ${ID_Text}
    Set Global Variable    ${ID_Text}
    Log To Console    ${ID_Count}
    : FOR    ${INDEX}    IN RANGE    1    ${ID_Count}+1
    \    Log To Console    ${INDEX}
    \    Set Global Variable    ${INDEX}
    \    ${ID}=    Get Text    xpath=(${ID_Text})[${INDEX}]
    \    log to console    ${ID}
    \    Set Global Variable    ${ID}
    \    Run Keyword If    '${ID}' == 'Current Address'    Run Keyword    Address_Check BOX
    \    Run Keyword If    '${ID}' == 'Permanent Address'    Run Keyword    Address_Check BOX

Address_Check BOX
    ${Check_Box}=    set variable    //div[@ng-if="check.checkname=='address'"]/ul/div/li/input
    ${Count}=    Get matching xpath count    ${Check_Box}
    Set Global Variable    ${Check_Box}
    #Log To Console    ${INDEX}
    #Log To Console    ${Count}
    click element    xpath=(${Check_Box})[${INDEX}]

Address_Radio Selection
    ${ID_Text}=    set variable    //div[@ng-if="check.checkname=='address'"]/ul/li
    ${ID_Count}=    Get matching xpath count    ${ID_Text}
    Set Global Variable    ${ID_Text}
    Log To Console    ${ID_Count}
    : FOR    ${INDEX}    IN RANGE    1    ${ID_Count}+1
    \    Log To Console    ${INDEX}
    \    Set Global Variable    ${INDEX}
    \    ${ID}=    Get Text    xpath=(${ID_Text})[${INDEX}]
    \    log to console    ${ID}

Employment_Check Selection
    ${ID_Text}=    set variable    //div[@ng-if="check.checkname=='employment'"]/ul/li
    ${ID_Count}=    Get matching xpath count    ${ID_Text}
    Set Global Variable    ${ID_Text}
    Log To Console    ${ID_Count}
    : FOR    ${INDEX}    IN RANGE    1    ${ID_Count}+1
    \    Log To Console    ${INDEX}
    \    Set Global Variable    ${INDEX}
    \    ${ID}=    Get Text    xpath=(${ID_Text})[${INDEX}]
    \    log to console    ${ID}
    \    Set Global Variable    ${ID}
    \    Run Keyword If    '${ID}' == 'Latest/Current'    Run Keyword    Employment_Check BOX
    \    Run Keyword If    '${ID}' == 'Last job(s)'    Run Keyword    Employment_Check BOX
    \    Run Keyword If    '${ID}' == 'Last year(s)'    Run Keyword    Employment_Check BOX
    \    Run Keyword If    '${ID}' == 'All'    Run Keyword    Employment_Check BOX
    \    Run Keyword If    '${ID}' == 'Last job(s) year(s)'    Run Keyword    Employment_Check BOX

Employment_Check BOX
    ${Check_Box}=    set variable    //div[@ng-if="check.checkname=='employment'"]/ul/li/input[1]
    ${Count}=    Get matching xpath count    ${Check_Box}
    Set Global Variable    ${Check_Box}
    #Log To Console    ${INDEX}
    #Log To Console    ${Count}
    click element    xpath=(${Check_Box})[${INDEX}]

ID Selection
    ${ID_Text}=    set variable    //div[@ng-if="check.checkname=='id'"]/ul/div
    ${ID_Count}=    Get matching xpath count    ${ID_Text}
    Set Global Variable    ${ID_Text}
    Log To Console    ${ID_Count}
    : FOR    ${INDEX}    IN RANGE    1    ${ID_Count}+1
    \    Log To Console    ${INDEX}
    \    Set Global Variable    ${INDEX}
    \    ${ID}=    Get Text    xpath=(${ID_Text})[${INDEX}]
    \    log to console    ${ID}
    \    Set Global Variable    ${ID}
    \    Run Keyword If    '${ID}' == 'Aadhaar Card'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'Driving Licence'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'Voters ID'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'National Insurance Card'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'National ID Card'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'PAN Card'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'Passport'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'Ration card'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'Social Security Number'    Run Keyword    ID_Check BOX
    \    Run Keyword If    '${ID}' == 'Social Insurance Number'    Run Keyword    ID_Check BOX

ID_Check BOX
    ${Check_Box}=    set variable    //div[@ng-if="check.checkname=='id'"]/ul/div/input    #//input[@ng-model='compo.selected']
    ${Count}=    Get matching xpath count    ${Check_Box}
    Set Global Variable    ${Check_Box}
    #Log To Console    ${INDEX}
    #Log To Console    ${Count}
    click element    xpath=(${Check_Box})[${INDEX}]
