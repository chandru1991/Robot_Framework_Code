Contract = "//span[text()='Contract']"
Client_Select = "//*[@id='client_chzn']/a/span"

ID_contract = "//span[text()='ID ']"
Specific = "//input[@name='Specific']"
Anyselected = "//input[@name='Anyselected']"
Select_count = "//select[@ng-model='check.idValue']"
option_1 = "//select[@ng-model='check.idValue']/option[text()='1']"
option_2 = "//select[@ng-model='check.idValue']/option[text()='2']"
option_3 = "//select[@ng-model='check.idValue']/option[text()='3']"
Check_TAT = "//*[@id='ctedu']"
ID_Check_TAT = "//input[@class='tat idTAT']"
ID_contract_check = "//div/li/input[@class='ng-pristine ng-valid']"
ID_Text = "//div[@class='ng-binding ng-scope']"

Education_Contract = "//span[text()='Education ']"

Reference_Contract = "//span[text()='Reference ']"

Address_Contract = "//span[text()='Address ']"
Past_Years = "//input[@value='pastyears']"
#Past_Years_radio = "//div[@ng-if="check.checkname=='address'"]/ul/li/input[@value="pastyears"]" 
#Past_Years_count = //select[@ng-model="check.pastyears"]/option[contains(text(),'3')]
#Past_Years_selection = //div[@ng-if="check.checkname=='address'"]/ul/li/select[@ng-model="check.pastyears"]
Last_Address = "//input[@value='lastaddresses']"
#Last_Addres_count = //select[@ng-model="check.lastaddresses"]/option[contains(text(),'3')][1]
#Last_Years_selection = //div[@ng-if="check.checkname=='address'"]/ul/li/select[@ng-model="check.lastaddresses"]

Criminal_Contract = "//span[text()='Criminal ']"

Employment_Contract = "//span[text()='Employment ']"

