casemanager_button="//span[text()='Case Manager']"
assigncase_button="//a[text()='Assign cases']"
assign_button="//button[text()='Assign']"
getname_path="//td[@class='sorting_1']"
search_button_ca="//button[text()='Search']"
clear_button="//button[text()='Clear']"	
getrefnumber="//td[@class='sorting_1']/preceding-sibling::td[2]"
clientselection_ca="//*[@id='client_chzn']/ul/li/input"
client_qa='Demo Include'
project_qa='Include Test'
getclientname="//td[@class='sorting_1']/following-sibling::td[1]"
getcheckname="//div[@class='ng-binding ng-scope']"
pagedropdownbox="//select[@name='caseAssignmentTable_length']"
pagesize="//option[text()='100']"
casecount='id=caseAssignmentTable_info'
componentsize="//tr[@ng-click='openFile(file)']"
getcomponent="//tr[@ng-click='openFile(file)']/td[1]/span"
closepopup="//button[@class='close']"
clickcase="//div[@class='ng-binding ng-scope']/input"
vendordropdown="//*[@id='vendor_chzn']/a/div/b"
vendorselection="//*[@id='vendor_chzn']/div/div/input"
vendornameinput='Infosys'
vendornotselected_alert='Please select a vendor.'
casenotselected_alert='Please select at least one case.'
checkmodification_alert="You have added/removed one or more checks. Click on search before start assigning."
firstcase="//tr[@role='row']/td/input"
addresscheck="xpath=(//input[@type='checkbox'])[3]"



