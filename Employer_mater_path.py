EMP_DB_Company = "//input[@id='column0']"
EMP_DB_Status = "//select[@id='column1']"
EMP_DB_City = "//input[@id='column2']"
EMP_DB_Search = "//button[@id='searchBtn']"
EMP_DB_Clear = "//button[@id='clrBtn']"
EMP_DB_AddNewComp = "//button[contains(text(),'Add New Company')]"
EMP_EmpName = "//input[@ng-model='company.general.name']"
EMP_EmpDesc = "//input[@ng-model='company.general.description']"
EMP_WebSite = "//input[@id='website']"
EMP_Contact = "//input[@id='generalcontact']/following-sibling::div/input"
EMP_Fax = "//input[@id='generalfax']/following-sibling::div/input"
EMP_Status = "//select[@ng-model='company.general.status']"
EMP_AddNewContact =  "//button[contains(text(),'Add New Contact')]"
EMP_ADD_ContactName = "//input[@ng-model='contact.contactname']"
EMP_ADD_Desgination = "//input[@ng-model='contact.designation']"
EMP_ADD_Contact = "//input[@id='contact']/following-sibling::div/input"
EMP_ADD_FaxNo = "//input[@id='faxNo']/following-sibling::div/input"
EMP_ADD_Mail = "//input[@id='mail']/following-sibling::div/input"
EMP_ADD_Country = "//div[@id='country1_chzn']/a/span"
EMP_ADD_CountryText = "//div[@id='country1_chzn']/div/div/input"
EMP_ADD_Pincode = "//input[@id='contactPincode']"
EMP_ADD_State = "//input[@ng-model='contact.state']"
EMP_ADD_City = "//input[@ng-model='contact.city']"
EMP_ADD_Address = "//input[@ng-model='contact.address']"
EMP_ADD_LandMark = "//input[@ng-model='contact.landmark']"
EMP_ADD_Save = "(//button[contains(text(),'Save')])[2]"
EMP_PaymentMode = "//select[@id='paymentmode']/following-sibling::p/label"
EMP_PaymentModeTxt = "//div[@class='optWrapper multiple open']/ul/li"
EMP_CheckBoxCount = "//ul[@class='options']/li/span/i"
EMP_DDInFavourOf = "//input[@ng-model='company.payment.ddinfavour']"
EMP_PayableAt = "//input[@ng-model='company.payment.ddpayableat']"
EMP_CheckInFavourOf = "//input[@ng-model='company.payment.chequename']"
EMP_Currency = "//div[@id='paymentcurrency_chzn']/a/span"
EMP_CUrrencyText = "//div[@id='paymentcurrency_chzn']/div/div/input"
EMP_VerificationFee = "//input[@ng-model='company.payment.fee']"
EMP_Country = "//div[@id='country_chzn']/a/span"
EMP_Pincode = "//input[@id='pincode']"
EMP_State = "//div[@id='state_chzn']/a/span"
EMP_AddressLine = "//input[@ng-model='company.general.address']"
EMP_LandMark = "//input[@ng-model='company.general.landmark']"
EMP_Prefered = "//input[@id='prefered']"
EMP_Mandatory = "//input[@id='mandatory']"
EMP_VerificationMode = "//div[@id='verficationmodeman_chzn']/a/div/b"
EMP_VerificationModeText = "//div[@id='verficationmodeman_chzn']/div/div/input"
EMP_EmailTemplate = "//select[@id='templates']/following-sibling::p/label"
EMP_EmailText = "//div[@class='optWrapper multiple open']/ul/li"
EMP_EmailCheck = "//div[@class='optWrapper multiple open']/ul/li/span/i"
EMP_DocumentPreference = "//div[@id='docPref_chzn']/a"
EMP_DocumentPreferenceText = "//div[@id='docPref_chzn']/div/div/input" 
EMP_DocumentList = "//select[@id='docs']/following-sibling::p/label"
EMP_DocumentText = "//div[@class='optWrapper multiple open']/ul/li"
EMP_DocumentCheck = "//div[@class='optWrapper multiple open']/ul/li/span/i"
EMP_Save = "//button[contains(text(),'Save')]"
