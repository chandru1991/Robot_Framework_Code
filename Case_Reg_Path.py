Case_Registration= "//span[text()='Case Registration']"
Register_New_Case= "//button[contains(text(),'Register New Case')]"
Client_Project="//a[@class='chzn-single chzn-default']"
Client_Location="//input[@type='text']"
Client_Select="//ul[@class='chzn-results']"
Data_Entry='id=dataentryBy'
Candidate="//option[contains(text(),'Candidate')]"
Service_Provider="//option[contains(text(),'Service Provider')]"
First_Name_Location='id=fname'
Last_Name_Location='id=lname'
Date_Of_Birth='id=dob'
Month="//strong[@class='ng-binding']"
Years="//button[@class='btn btn-default btn-sm btn-block']"
Year="//button[@ng-click='move(-1)']"
Select_Year="//span[contains(text(),'2000')]"
Select_Month="//span[contains(text(),'January')]"
Select_Day="//span[contains(text(),'03')]"
Gender_Location="//select[@id='gender']"
Gender_Select="//option[contains(text(),'Male')]"
Email_Location='id=email'
Father_First_Name_Location="//input[@ng-model='case.fatherFirstName']"
Father_Last_Name_Location="//input[@ng-model='case.fatherLastName']"
Marital_Status="//select[@id='maritalstatus']"
Marital_Status_Select="//option[contains(text(),'Single')]"
Client_ID='id=clientidtype'
Employe_ID_Location='id=employeId'
Landline_Number_Location="//input[@id='landLineNumber']/following-sibling::div/input"
Mobile_Number_Location="//input[@id='contactnum']/following-sibling::div/input"
Priority='id=priorty'
Priority_Select="xpath=//option[contains(text(),'High')]"
HR_MailID="//input[@id='hrEmail']/following-sibling::div/input"
HR_mailcount= "//input[@id='hrEmail']/following-sibling::div"
checkBOx="//input[@type='checkbox']"
Insuff="//label[@for='Highest 1insuff']"
Insuff_Comment_Location='id=Highest 1comments'
YTR_Ref	="//table[@id='table']/tbody[2]/tr[2]/td[3]/label"
YTR_Ref_Comment="//textarea[@id='Reference 1comments']"
YTR_Emp	="//table[@id='table']/tbody[3]/tr[2]/td[3]/label"
YTR_Emp_Comment= "//textarea[@id='Employment 1comments']"
RP="//label[@for='Current Addressrp']"
RP_Comment_Location='id=Current Addresscomments'
DataBase_CheckAll="//input[@ng-model='dbselectAll']"
checkbox= "//input[@ng-model='compo.selected']"
Case_Document="//button[contains(text(),'Case Document(s)')]"
upload_button="//input[@class='btn ng-pristine ng-valid']"
componenttype='id=componenttype'
component_option="//option[contains(text(),'General')]"
documenttype='id=documenttype'
document_option="//option[contains(text(),'LOA')]"
document_download="//span[@class='glyphicon glyphicon-download']"
document_delete="//span[@class='glyphicon glyphicon-remove-circle']"
Save="//button[contains(text(),'Save')]"
Save_ok="//button[contains(text(),'OK')]"
Save_Submit="//button[contains(text(),'Save & Submit')]"
Upload_Ok='id=btnUpload'
Alert_ok='class=btn save-button'
Alert_Message="//div[@class='message']"
Password_Alert="//button[@class='btn save-button']"
Conformation_Alert_yes="//button[text()='Yes']"
Conformation_Alert_No="//button[text()='No']"
Email_Count="//spam[@class='tag label label-success']"
LandLine_Count="//input[@id='landLineNumber']/following-sibling::div/span"
Mobile_Count="//input[@id='contactnum']/following-sibling::div/span"
DB_Firstname='id=column1'
DB_Lastname='id=column2'
DB_Client="//ul[@class='chzn-choices']"
DB_Cli_Text="//input[@class='default']"
DB_Cli_Selc="//ul[@class='chzn-results']"
DB_REFNO="//tr[starts-with(@class,'odd')]/td[1]"
DB_Re_init= "//span[@class='glyphicon glyphicon-retweet']"
Re_init_Refno= "//input[@id='reinitateRefNO']"
Re_init_fname= "//input[@id='reinitiateFname']"
Re_init_lname= "//input[@id='reinitiateLname']"
Re_init_email= "//input[@id='email']"
Re_init_notifmail= "//input[@id='hrReiniitateEmail']"
Re_resent= "//button[text()='Resend']"
Re_cancel= "//button[text()='Cancel']"
Re_mail_del= "//input[@id='hrReiniitateEmail']/following-sibling::div/span/span[@data-role='remove']"
Re_mail_new= "//*[@id='modalScope']/div[2]/div[2]/div"
Client_Approval_Yes= "//input[@id='approvalYes']"
Client_Approval_Val= "//input[@id='approvalYes' and @value='true']"
Client_Approval_No= "//input[@id='id=approvalNo']"
Home = '''//li[@ng-if="user.type != 'clientuser'"][1]/a/i'''

