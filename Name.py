import names

class Name(object):

    def FirstName(self):
        self.Firstname = names.get_first_name()
        return self.Firstname

    def LastName(self):
        self.Lastname = names.get_last_name()
        return self.Lastname

def main():
    FR = Name()
    FR.FirstName(FR.Firstname)
    print(FR.Firstname)
    FR.LastName(FR.Lastname)
    print(FR.Lastname)

if __name__ == '__main__':
    main()  

# names.get_full_name()
# names.get_full_name(gender='male')
# names.get_first_name()
# names.get_first_name(gender='female')
# names.get_last_name()
