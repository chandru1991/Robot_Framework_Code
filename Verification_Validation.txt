*** Settings ***
Default Tags      Verification_Validation
Resource          Library.robot
Resource          Keywords.robot
Resource          Common_Lib_Var.robot

*** Test Cases ***
TC_00_Verification Supervision
    Login
    Random String
    Random String Search values
    click element    ${Verification_Supervision}
    #click element    ${Verification}
    sleep    3s
    click element    ${VERS_DB_CaseRefNo}
    input text    ${VERS_DB_CaseRefNo}    001JUPITOR001213
    click element    ${VERS_DB_Client}
    sleep    1s
    click element    ${VERS_DB_ClientSearch}
    input text    ${VERS_DB_ClientSearch}    Jupitor
    sleep    2s
    Press Key    ${VERS_DB_ClientSearch}    \\13
    click element    ${VERS_DB_FirstName}
    input text    ${VERS_DB_FirstName}    ${TEMP_FIRST_NAME}
    click element    ${VERS_DB_LastName}
    input text    ${VERS_DB_LastName}    ${TEMP_LAST_NAME}
    sleep    2s
    click element    ${VERS_DB_RegFrmDate}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    sleep    2s
    click element    ${VERS_DB_RegToDate}
    click element    xpath=(${Month})[2]
    #click element    xpath=(${Years})[2]
    click element    xpath=(${Year})[2]
    #click element    xpath=(${Select_Year})[2]
    click element    ${Select_Month}
    click element    ${Select_Day}
    #click element    ${VERS_DB_RegInDate}
    #sleep    2s
    #click element    ${Month}
    #sleep    2s
    #click element    ${Years}
    #sleep    2s
    #click element    ${Year}
    #sleep    2s
    #click element    ${Select_Year}
    #sleep    2s
    #click element    ${Select_Month}
    #sleep    2s
    #click element    ${Select_Day}
    #sleep    2s
    #click element    ${VERS_DB_InFrmDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_InToDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_WrkStFrmDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_WrkStToDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_VrInitiFrmDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_VrInitiToDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_DueFromDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_DueToDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    click element    ${VERS_DB_CompWrkFlw}
    click element    ${VERS_DB_CompOptRe_Init}
    sleep    2s
    click element    ${VERS_DB_CheckTyp}
    click element    ${VERS_DB_CheckEmp}
    sleep    2s
    #click element    ${VERS_DB_VerifyMode}
    #click element    ${VERS_DB_VerInPerson}
    sleep    2s
    click element    ${VERS_DB_CaseSource}
    click element    ${VERS_DB_CasSourCandt}
    sleep    2s
    click element    ${VERS_DB_Status}
    click element    ${VERS_DB_VerFax}
    sleep    1s
    click element    ${VERS_DB_CompType}
    input text    ${VERS_DB_CompType}    Database

TC_01_Verification
    Login
    Random String
    Random String Search values
    click element    ${Verification}
    sleep    3s
    click element    ${VERS_DB_CaseRefNo}
    input text    ${VERS_DB_CaseRefNo}    001JUPITOR001213
    click element    ${VERS_DB_Client}
    sleep    1s
    click element    ${VERS_DB_ClientSearch}
    input text    ${VERS_DB_ClientSearch}    Jupitor
    sleep    2s
    Press Key    ${VERS_DB_ClientSearch}    \\13
    click element    ${VERS_DB_FirstName}
    input text    ${VERS_DB_FirstName}    ${TEMP_FIRST_NAME}
    click element    ${VERS_DB_LastName}
    input text    ${VERS_DB_LastName}    ${TEMP_LAST_NAME}
    sleep    2s
    click element    ${VERS_DB_RegFrmDate}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    sleep    2s
    #click element    ${VERS_DB_RegToDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_RegInDate}
    #sleep    2s
    #click element    ${Month}
    #sleep    2s
    #click element    ${Years}
    #sleep    2s
    #click element    ${Year}
    #sleep    2s
    #click element    ${Select_Year}
    #sleep    2s
    #click element    ${Select_Month}
    #sleep    2s
    #click element    ${Select_Day}
    #sleep    2s
    #click element    ${VERS_DB_InFrmDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_InToDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_WrkStFrmDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_WrkStToDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_VrInitiFrmDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_VrInitiToDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_DueFromDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    #click element    ${VERS_DB_DueToDate}
    #click element    ${Month}
    #click element    ${Years}
    #click element    ${Year}
    #click element    ${Select_Year}
    #click element    ${Select_Month}
    #click element    ${Select_Day}
    click element    ${VERS_DB_CompWrkFlw}
    click element    ${VERS_DB_CompOptRe_Init}
    sleep    2s
    click element    ${VERS_DB_CheckTyp}
    click element    ${VERS_DB_CheckEmp}
    sleep    2s
    click element    ${VERS_DB_VerifyMode}
    click element    ${VERS_DB_VerInPerson}
    sleep    3s
    click element    ${VER_DB_CaseSource}
    click element    ${VERS_DB_CasSourCandt}
    sleep    2s
    click element    ${VER_DB_Status}
    click element    ${VERS_DB_VerFax}
    sleep    1s
    click element    ${VER_DB_CompType}
    input text    ${VER_DB_CompType}    Database
