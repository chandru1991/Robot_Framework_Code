*** Keywords ***
YTR Raise
    sleep    3s
    click element    ${YTR_Clear}
    sleep    2s
    click element    ${INSUFF_CaseRefNo}
    input text    ${INSUFF_CaseRefNo}    ${RefNo1}
    sleep    2s
    click element    ${INSUFF_FirstName}
    input text    ${INSUFF_FirstName}    ${FName}
    sleep    2s
    click element    ${INSUFF_LastName}
    input text    ${INSUFF_LastName}    ${LName}
    sleep    2s
    click element    ${Search_Button}
    sleep    3s
    ${RefNo2}=    Get Text    ${YTR_CaseRefNo}
    log to console    ${RefNo2}
    Run Keyword If    '${RefNo1}'=='${RefNo2}'    click element    //tr[@role='row']/td[1]
    sleep    3s
    #click element    ${Case_Document}
    click element    //button[contains(text(),'Case Documents(s)')]
    sleep    3s
    choose file    ${upload_button}    ${File1}
    wait until element is enabled    ${documenttype}
    click element    ${documenttype}
    Select From List By Label    ${documenttype}    Others
    sleep    3s
    click element    ${document_download}
    click element    ${Upload_Ok}
    sleep    2s
    click element    ${Save_ok}
    sleep    2s
    #click element    ${Case_Document}
    click element    //button[contains(text(),'Case Documents(s)')]
    sleep    2s
    click element    ${document_delete}
    wait until element is enabled    ${Conformation_Alert_yes}
    Element Text Should Be    ${Alert_Message}    ${document_delete_Alert}
    click element    ${Conformation_Alert_yes}
    sleep    2s
    click element    ${Upload_Ok}
    sleep    2s
    click element    ${Save_ok}
