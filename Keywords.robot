*** Settings ***
Variables         login.py
Library           Name.py
Library           BuiltIn

*** Keywords ***
Browser - Headless
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    headless
    Call Method    ${chrome_options}    add_argument    disable-gpu
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size    1920    1080

Openworld Portal
    Go To    ${SP_URL}
    input text    ${usernamepath}    ${ValidUsername_1}
    input password    ${passwordpath}    ${ValidPassword_1}
    Click button    ${loginbutton}

Openworld Portal 2
    Go To    ${SP_URL}
    input text    ${usernamepath}    ${ValidUsername_2}
    input password    ${passwordpath}    ${ValidPassword_2}
    Click button    ${loginbutton}

Openworld Portal Admin
    Go To    ${SP_URL}
    input text    ${usernamepath}    ${admin_ValidUsername_1}
    input password    ${passwordpath}    ${admin_ValidPassword_1}
    Click button    ${loginbutton}

Openworld Portal - Client
    Go To    ${Client_URL}
    input text    ${usernamepath}    ${Client_ValidUsername_1}
    input password    ${passwordpath}    ${Client_ValidPassword_1}
    Click button    ${loginbutton}

Gmail
    Go To    ${GMAIL}

Service Provider Admin Login - portal
    Open Browser    ${SP_URL}    ${Browser}
    Maximize Browser Window
    input text    ${usernamepath}    ${admin_ValidUsername_1}
    input password    ${passwordpath}    ${admin_ValidPassword_1}
    Click button    ${loginbutton}

Service Provider Login - portal
    Open Browser    ${SP_URL}    ${Browser}
    Maximize Browser Window
    input text    ${usernamepath}    ${ValidUsername_1}
    input password    ${passwordpath}    ${ValidPassword_1}
    Click button    ${loginbutton}

Service Provider Login 2 - portal
    Open Browser    ${SP_URL}    ${Browser}
    Maximize Browser Window
    input text    ${usernamepath}    ${ValidUsername_2}
    input password    ${passwordpath}    ${ValidPassword_2}
    Click button    ${loginbutton}

Client Login - portal
    Open Browser    ${Client_URL}    ${Browser}
    Maximize Browser Window
    input text    ${usernamepath}    ${Client_ValidUsername_1}
    input password    ${passwordpath}    ${Client_ValidPassword_1}
    Click button    ${loginbutton}

Service Provider Login - mail
    Open Browser    ${GMAIL}    ${Browser}
    Maximize Browser Window

Random String New
    ${randomfirstname} =    FirstName
    ${randomlastname} =    LastName
    Set Global Variable    ${randomfirstname}
    Set Global Variable    ${randomlastname}
    #${randomfirstname}    Generate Random String    8    qwertyuioplkjhgfdsazxcvbnm
    #${randomlastname}    Generate Random String    5    qwertyuioplkjhgfdsazxcvbnm
    #Log    ${randomfirstname}
    #Set Global Variable    ${randomfirstname}
    #Set Global Variable    ${randomlastname}

Click Database
    ${xpath}=    Set Variable    ${checkbox_selection_path}
    Click Element    xpath=(${xpath})[${i}]
    Click Element    ${save_submit_path}
    sleep    3s
    Element Text Should Be    ${message_path}    ${messagedb_Text_alert}
    Click Element    ${no_path}
    Page Should Contain    Database
    Sleep    3s
    Click Element    ${save_submit_path}
    sleep    3s
    Element Text Should Be    ${message_path}    ${messagedb_Text_alert}
    Click Element    ${yes_path}
    sleep    3s
    Element Text Should Be    ${message_path}    ${casesave_alert}
    Sleep    3s
    Click Element    ${ok_path}
    Sleep    3s
    Click Element    ${verificationsupervision_path}
    Sleep    3s
    Wait Until Element Is Visible    ${clientfilter_path}
    Input Text    ${clientfilter_path}    ${clientname}
    Press Key    ${clientfilter_path}    \\13
    Input Text    ${firstnamefilter_path}    ${randomfirstname}
    Input Text    ${lastnamefilter_path}    ${randomlastname}
    sleep    3s
    Click Element    ${filtersearch_path}
    sleep    3s
    Clear Element Text    ${firstnamefilter_path}
    Clear Element Text    ${lastnamefilter_path}
    Click Element    ${clientfilterclose_path}
    ${firstletter}=    Get substring    ${randomfirstname}    0    1
    ${upperletter}=    Convert To Uppercase    ${firstletter}
    ${nextletters}=    Get substring    ${randomfirstname}    1
    ${randomfirstname}=    Catenate    ${upperletter}${nextletters}
    ${firstletter}=    Get substring    ${randomlastname}    0    1
    ${upperletter}=    Convert To Uppercase    ${firstletter}
    ${nextletters}=    Get substring    ${randomlastname}    1
    ${randomlastname}=    Catenate    ${upperletter}${nextletters}
    sleep    3s
    Page Should Contain    ${randomfirstname}
    sleep    3s
    Page Should Contain    ${randomlastname}
    sleep    3s
    Page Should Contain    ${clientname}
    sleep    3s
    Log To Console    Case Available in Verification Stage
    Close Browser

Select Checkbox
    ${xpath}=    Set Variable    ${checkbox_selection_path}
    Click Element    xpath=(${xpath})[${i}]

Select Checkbox Count
    ${xpath}=    Set Variable    ${checkbox_selection_path}
    Click Element    xpath=(${xpath})[${i}]
    ${value_count}=    Evaluate    ${value_count}+1
    Set Global Variable    ${value_count}

Checkbox Checked
    ${xpath}=    Set Variable    ${checkbox_selection_path}
    Checkbox Should Be Selected    xpath=(${xpath})[${i}]

Checkbox Unchecked
    ${xpath}=    Set Variable    ${checkbox_selection_path}
    Checkbox Should Not Be Selected    xpath=(${xpath})[${i}]

Select Checkbox Database
    ${xpath}=    Set Variable    ${checkbox_selection_path}
    ${status}=    Run Keyword And Return Status    Checkbox Should be Selected    xpath=(${xpath})[${i}]
    Run Keyword if    '${status}' != 'True'    Select Checkbox
    Click Element    ${databasecomponent_path}
    ${status}=    Run Keyword And Return Status    Checkbox Should be Selected    ${databaseselectall_path}
    Run Keyword if    '${status}' == 'True'    Fail
    Click Element    ${save_path}
    Wait Until Element Is Visible    ${ok_path}
    sleep    3s
    Click Element    ${ok_path}
    Input Text    ${firstnamefilter_path}    ${randomfirstname}
    Input Text    ${lastnamefilter_path}    ${randomlastname}
    Wait Until Element Is Visible    ${clientfilter_path}
    Input Text    ${clientfilter_path}    ${clientname}
    Press Key    ${clientfilter_path}    \\13
    Click Element    ${filtersearch_path}
    Sleep    3s
    Wait Until Element Is Visible    ${click_case_path}
    Click Element    ${click_case_path}
    ${status}=    Run Keyword And Return Status    Checkbox Should be Selected    ${databaseselectall_path}
    Run Keyword if    '${status}' == 'True'    Fail
    ${status}=    Run Keyword And Return Status    Checkbox Should be Selected    ${databasecomponent_path}
    Run Keyword if    '${status}' == 'True'    Fail
    Click Element    ${databaseselectall_path}
    Click Element    ${save_path}
    Sleep    2s
    Wait Until Element Is Visible    ${ok_path}
    Click Element    ${ok_path}
    Input Text    ${firstnamefilter_path}    ${randomfirstname}
    Input Text    ${lastnamefilter_path}    ${randomlastname}
    Wait Until Element Is Visible    ${clientfilter_path}
    Input Text    ${clientfilter_path}    ${clientname}
    Press Key    ${clientfilter_path}    \\13
    Click Element    ${filtersearch_path}
    Sleep    2s
    Wait Until Element Is Visible    ${click_case_path}
    Click Element    ${click_case_path}
    ${status}=    Run Keyword And Return Status    Checkbox Should be Selected    ${databaseselectall_path}
    Run Keyword if    '${status}' == 'False'    Fail
    Click Element    ${save_path}
    Sleep    3s
    Wait Until Element Is Visible    ${ok_path}
    Click Element    ${ok_path}

Uncheck Select All
    Checkbox Should not be Selected    ${selectallcomponent_path}

Check Select All
    Checkbox Should be Selected    ${selectallcomponent_path}

Check Components in a Case
    ${stringvalue}=    Get Text    ${casecount}
    ${separatestring}=    Split String    ${stringvalue}
    #Log To Console    @{separatestring}[5]
    ${separateint}=    Convert To Integer    @{separatestring}[5]
    #Log To Console    ${separateint}
    Set Global Variable    ${separateint}
    Run Keyword If    ${separateint} > 100    Pagination Count
    Pagination Count

Pagination Count
    Click Element    ${pagedropdownbox}
    Click Element    ${pagesize}
    Sleep    2s
    ${matchingcount_check}=    Get Matching Xpath Count    ${getrefnumber}
    Set Global Variable    ${matchingcount_check}
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcount_check}+1
    \    Click Element    xpath=(${getrefnumber})[${INDEX}]
    \    Sleep    2s
    \    ${matchingcomponent}=    Get Matching Xpath Count    ${componentsize}
    \    Set Global Variable    ${matchingcomponent}
    \    Run Keyword If    ${matchingcomponent} >= 1    Available Component
    Output Result

Available Component
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result_education}=    Run Keyword And Return Status    List Should Contain Value    ${educationcomponent}    ${component}
    \    #Log To Console    ${result_education}
    \    Run Keyword If    ${result_education} == True    Set Education Check Value
    \    Exit For Loop If    ${result_education} == True
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result_employment}=    Run Keyword And Return Status    List Should Contain Value    ${employmentcomponent}    ${component}
    \    #Log To Console    ${result_employment}
    \    Run Keyword If    ${result_employment} == True    Set Employment Check Value
    \    #Log To Console    Employment Result ${true_employment}
    \    Exit For Loop If    ${result_employment} == True
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result_reference}=    Run Keyword And Return Status    List Should Contain Value    ${referencecomponent}    ${component}
    \    #Log To Console    ${result_employment}
    \    Run Keyword If    ${result_reference} == True    Set Reference Check Value
    \    #Log To Console    Employment Result ${true_employment}
    \    Exit For Loop If    ${result_reference} == True
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result_ID}=    Run Keyword And Return Status    List Should Contain Value    ${IDcomponent}    ${component}
    \    #Log To Console    ${result_employment}
    \    Run Keyword If    ${result_ID} == True    Set ID Check Value
    \    #Log To Console    Employment Result ${true_employment}
    \    Exit For Loop If    ${result_ID} == True
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result_address}=    Run Keyword And Return Status    List Should Contain Value    ${addresscomponent}    ${component}
    \    #Log To Console    ${result_employment}
    \    Run Keyword If    ${result_address} == True    Set Address Check Value
    \    #Log To Console    Employment Result ${true_employment}
    \    Exit For Loop If    ${result_address} == True
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result_database}=    Run Keyword And Return Status    List Should Contain Value    ${databasecomponent}    ${component}
    \    #Log To Console    ${result_employment}
    \    Run Keyword If    ${result_database} == True    Set Database Check Value
    \    #Log To Console    Employment Result ${true_employment}
    \    Exit For Loop If    ${result_database} == True
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result_criminal}=    Run Keyword And Return Status    List Should Contain Value    ${criminalcomponent}    ${component}
    \    #Log To Console    ${result_employment}
    \    Run Keyword If    ${result_criminal} == True    Set Criminal Check Value
    \    #Log To Console    Employment Result ${true_employment}
    \    Exit For Loop If    ${result_criminal} == True
    Click Element    ${closepopup}
    Sleep    2s

Set Education Check Value
    ${true_education}=    Set Variable    True
    Set Global Variable    ${true_education}

Set Employment Check Value
    ${true_employment}=    Set Variable    True
    Set Global Variable    ${true_employment}

Set Reference Check Value
    ${true_reference}=    Set Variable    True
    Set Global Variable    ${true_reference}

Set ID Check Value
    ${true_ID}=    Set Variable    True
    Set Global Variable    ${true_ID}

Set Address Check Value
    ${true_address}=    Set Variable    True
    Set Global Variable    ${true_address}

Set Database Check Value
    ${true_database}=    Set Variable    True
    Set Global Variable    ${true_database}

Set Criminal Check Value
    ${true_criminal}=    Set Variable    True
    Set Global Variable    ${true_criminal}

Output Result
    Log To Console    Available Components Listed Below:
    ${education_ouput}=    Run Keyword if    ${true_education} == True    Set Variable    Education Check is Available
    Log To Console    ${education_ouput}
    ${employment_ouput}=    Run Keyword if    ${true_employment} == True    Set Variable    Employment Check is Available
    Log To Console    ${employment_ouput}
    ${reference_ouput}=    Run Keyword if    ${true_reference} == True    Set Variable    Reference Check is Available
    Log To Console    ${reference_ouput}
    ${ID_ouput}=    Run Keyword if    ${true_ID} == True    Set Variable    ID Check is Available
    Log To Console    ${ID_ouput}
    ${address_ouput}=    Run Keyword if    ${true_address} == True    Set Variable    Address Check is Available
    Log To Console    ${address_ouput}
    ${database_ouput}=    Run Keyword if    ${true_database} == True    Set Variable    Database Check is Available
    Log To Console    ${database_ouput}
    ${criminal_ouput}=    Run Keyword if    ${true_criminal} == True    Set Variable    Criminal Check is Available
    Log To Console    ${criminal_ouput}
    ${education_ouput}=    Run Keyword if    ${true_education} != True    Set Variable    Education Check Not Available
    Log To Console    ${education_ouput}
    ${employment_ouput}=    Run Keyword if    ${true_employment} != True    Set Variable    Employment Check Not Available
    Log To Console    ${employment_ouput}
    ${reference_ouput}=    Run Keyword if    ${true_reference} != True    Set Variable    Reference Check Not Available
    Log To Console    ${reference_ouput}
    ${ID_ouput}=    Run Keyword if    ${true_ID} != True    Set Variable    ID Check Not Available
    Log To Console    ${ID_ouput}
    ${address_ouput}=    Run Keyword if    ${true_address} != True    Set Variable    Address Check Not Available
    Log To Console    ${address_ouput}
    ${database_ouput}=    Run Keyword if    ${true_database} != True    Set Variable    Database Check Not Available
    Log To Console    ${database_ouput}
    ${criminal_ouput}=    Run Keyword if    ${true_criminal} != True    Set Variable    Criminal Check Not Available
    Log To Console    ${criminal_ouput}
    Log To Console    Result of Components:
    Log To Console    Education:${true_education}
    Log To Console    Employment:${true_employment}
    Log To Console    ID:${true_ID}
    Log To Console    Reference:${true_reference}
    Log To Console    Address:${true_address}
    Log To Console    Database:${true_database}
    Log To Console    Criminal:${true_criminal}

Check Unassigned Case
    Click Element    ${clear_button}
    Click Element    xpath=(${clickcase})[${INDEX}]
    Click Element    ${search_button_ca}
    Sleep    2s
    ${matchingcount_check}=    Get Matching Xpath Count    ${getrefnumber}
    Set Global Variable    ${matchingcount_check}
    Run Keyword If    ${matchingcount_check} == 0 and ${true_education} == False and '${checkname}' == 'Education'    No Case Available
    Run Keyword If    ${matchingcount_check} == 0 and ${true_employment} == False and '${checkname}' == 'Employment'    No Case Available
    Run Keyword If    ${matchingcount_check} == 0 and ${true_ID} == False and '${checkname}' == 'ID'    No Case Available
    Run Keyword If    ${matchingcount_check} == 0 and ${true_reference} == False and '${checkname}' == 'Reference'    No Case Available
    Run Keyword If    ${matchingcount_check} == 0 and ${true_address} == False and '${checkname}' == 'Address'    No Case Available
    Run Keyword If    ${matchingcount_check} == 0 and ${true_database} == False and '${checkname}' == 'Database'    No Case Available
    Run Keyword If    ${matchingcount_check} == 0 and ${true_criminal} == False and '${checkname}' == 'Criminal'    No Case Available
    Run Keyword If    ${matchingcount_check} == 0 and ${true_education} == True and '${checkname}' == 'Education'    Fail
    Run Keyword If    ${matchingcount_check} == 0 and ${true_employment} == True and '${checkname}' == 'Employment'    Fail
    Run Keyword If    ${matchingcount_check} == 0 and ${true_reference} == True and '${checkname}' == 'Reference'    Fail
    Run Keyword If    ${matchingcount_check} == 0 and ${true_ID} == True and '${checkname}' == 'ID'    Fail
    Run Keyword If    ${matchingcount_check} == 0 and ${true_address} == True and '${checkname}' == 'Address'    Fail
    Run Keyword If    ${matchingcount_check} == 0 and ${true_database} == True and '${checkname}' == 'Database'    Fail
    Run Keyword If    ${matchingcount_check} == 0 and ${true_criminal} == True and '${checkname}' == 'Criminal'    Fail
    Run Keyword If    ${matchingcount_check} >= 1 and '${checkname}' == 'Education'    Check Case Components
    Run Keyword If    ${matchingcount_check} >= 1 and '${checkname}' == 'Reference'    Check Case Components
    Run Keyword If    ${matchingcount_check} >= 1 and '${checkname}' == 'Employment'    Check Case Components
    Run Keyword If    ${matchingcount_check} >= 1 and '${checkname}' == 'Address'    Check Case Components
    Run Keyword If    ${matchingcount_check} >= 1 and '${checkname}' == 'Criminal'    Check Case Components
    Run Keyword If    ${matchingcount_check} >= 1 and '${checkname}' == 'ID'    Check Case Components
    Run Keyword If    ${matchingcount_check} >= 1 and '${checkname}' == 'Database'    Check Case Components

Check Case Components
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcount_check}+1
    \    Click Element    xpath=(${getrefnumber})[${INDEX}]
    \    Sleep    2s
    \    ${matchingcomponent}=    Get Matching Xpath Count    ${componentsize}
    \    Set Global Variable    ${matchingcomponent}
    \    Run Keyword If    ${matchingcomponent} > 1 and '${checkname}' == 'Education'    Education Components Count
    \    Run Keyword If    ${matchingcomponent} == 1 and '${checkname}' == 'Education'    Education Component Count
    \    Run Keyword If    ${matchingcomponent} > 1 and '${checkname}' == 'Reference'    Reference Components Count
    \    Run Keyword If    ${matchingcomponent} == 1 and '${checkname}' == 'Reference'    Reference Component Count
    \    Run Keyword If    ${matchingcomponent} > 1 and '${checkname}' == 'Employment'    Employment Components Count
    \    Run Keyword If    ${matchingcomponent} == 1 and '${checkname}' == 'Employment'    Employment Component Count
    \    Run Keyword If    ${matchingcomponent} > 1 and '${checkname}' == 'Address'    Address Components Count
    \    Run Keyword If    ${matchingcomponent} == 1 and '${checkname}' == 'Address'    Address Component Count
    \    Run Keyword If    ${matchingcomponent} > 1 and '${checkname}' == 'Criminal'    Criminal Components Count
    \    Run Keyword If    ${matchingcomponent} == 1 and '${checkname}' == 'Criminal'    Criminal Component Count
    \    Run Keyword If    ${matchingcomponent} > 1 and '${checkname}' == 'ID'    ID Components Count
    \    Run Keyword If    ${matchingcomponent} == 1 and '${checkname}' == 'ID'    ID Component Count
    \    Run Keyword If    ${matchingcomponent} > 1 and '${checkname}' == 'Database'    Database Components Count
    \    Run Keyword If    ${matchingcomponent} == 1 and '${checkname}' == 'Database'    Database Component Count
    \    #Click Element    //button[@class='close']
    \    Sleep    2s

Education Components Count
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${educationcomponent}    ${component}
    \    ${edu_true}=    Run Keyword If    ${result} == True    Set Variable    True
    \    #Log To Console    ${true}
    \    Exit For Loop If    ${edu_true} == True
    Click Element    ${closepopup}
    Set Global Variable    ${edu_true}
    #Run Keyword If    ${result} == 1    Fail

Education Component Count
    ${component}=    Get Text    ${getcomponent}
    #Log To Console    ${component}
    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${educationcomponent}    ${component}
    ${edu_true}=    Run Keyword If    ${result} == True    Set Variable    True
    Set Global Variable    ${edu_true}
    #Log To Console    ${true}
    Click Element    ${closepopup}
    #Run Keyword If    ${true} != True    Fail

Reference Components Count
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${referencecomponent}    ${component}
    \    ${ref_true}=    Run Keyword If    ${result} == True    Set Variable    True
    \    #Log To Console    ${true}
    \    Exit For Loop If    ${ref_true} == True
    Set Global Variable    ${ref_true}
    Click Element    ${closepopup}
    #Run Keyword If    ${result} == 1    Fail

Reference Component Count
    ${component}=    Get Text    ${getcomponent}
    #Log To Console    ${component}
    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${referencecomponent}    ${component}
    ${ref_true}=    Run Keyword If    ${result} == True    Set Variable    True
    Set Global Variable    ${ref_true}
    #Log To Console    ${true}
    Click Element    ${closepopup}

Address Components Count
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${addresscomponent}    ${component}
    \    ${add_true}=    Run Keyword If    ${result} == True    Set Variable    True
    \    #Log To Console    ${true}
    \    Exit For Loop If    ${add_true} == True
    Set Global Variable    ${add_true}
    Click Element    ${closepopup}

Address Component Count
    ${component}=    Get Text    ${getcomponent}
    #Log To Console    ${component}
    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${addresscomponent}    ${component}
    ${add_true}=    Run Keyword If    ${result} == True    Set Variable    True
    Set Global Variable    ${add_true}
    #Log To Console    ${true}
    Click Element    //button[@class='close']

Employment Components Count
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${employmentcomponent}    ${component}
    \    ${emp_true}=    Run Keyword If    ${result} == True    Set Variable    True
    \    #Log To Console    ${true}
    \    Exit For Loop If    ${emp_true} == True
    Set Global Variable    ${emp_true}
    Click Element    ${closepopup}

Employment Component Count
    ${component}=    Get Text    ${getcomponent}
    #Log To Console    ${component}
    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${employmentcomponent}    ${component}
    ${emp_true}=    Run Keyword If    ${result} == True    Set Variable    True
    Set Global Variable    ${emp_true}
    #Log To Console    ${true}
    Click Element    ${closepopup}

Criminal Components Count
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${criminalcomponent}    ${component}
    \    ${cri_true}=    Run Keyword If    ${result} == True    Set Variable    True
    \    #Log To Console    ${true}
    \    Exit For Loop If    ${cri_true} == True
    Set Global Variable    ${cri_true}
    Click Element    ${closepopup}

Criminal Component Count
    ${component}=    Get Text    ${getcomponent}
    #Log To Console    ${component}
    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${criminalcomponent}    ${component}
    ${cri_true}=    Run Keyword If    ${result} == True    Set Variable    True
    #Log To Console    ${true}
    Set Global Variable    ${cri_true}
    Click Element    ${closepopup}

ID Components Count
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${idcomponent}    ${component}
    \    ${id_true}=    Run Keyword If    ${result} == True    Set Variable    True
    \    #Log To Console    ${true}
    \    Exit For Loop If    ${id_true} == True
    Set Global Variable    ${id_true}
    Click Element    ${closepopup}

ID Component Count
    ${component}=    Get Text    ${getcomponent}
    #Log To Console    ${component}
    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${idcomponent}    ${component}
    ${id_true}=    Run Keyword If    ${result} == True    Set Variable    True
    #Log To Console    ${true}
    Set Global Variable    ${id_true}
    Click Element    ${closepopup}

Database Components Count
    : FOR    ${INDEX}    IN RANGE    1    ${matchingcomponent}+1
    \    #Log To Console    ${matchingcomponent}
    \    ${component}=    Get Text    xpath=(${getcomponent})[${INDEX}]
    \    #Log To Console    ${component}
    \    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${databasecomponent}    ${component}
    \    ${db_true}=    Run Keyword If    ${result} == True    Set Variable    True
    \    #Log To Console    ${true}
    \    Exit For Loop If    ${db_true} == True
    Set Global Variable    ${db_true}
    Click Element    ${closepopup}

Database Component Count
    ${component}=    Get Text    ${getcomponent}
    #Log To Console    ${component}
    ${result}=    Run Keyword And Return Status    List Should Contain Value    ${databasecomponent}    ${component}
    ${db_true}=    Run Keyword If    ${result} == True    Set Variable    True
    #Log To Console    ${true}
    Set Global Variable    ${db_true}
    Click Element    ${closepopup}

No Case Available
    Log To Console    No Case Available to Assign ${checkname} Check

No Case Available to Assign
    Log To Console    No Case Available to Assign

Do Data Entry
    ${countvalue} =    Evaluate    ${countvalue}+1
    Set Global Variable    ${countvalue}
    Data Entry Section
    #Wait Until Element Is Visible    ${DE_Dashboard}
    #Click Element    ${DE_Dashboard}
    DataEntry GetNext

Data Entry Section
    Log To Console    Ready To Do Data Entry
    ${component_count}    Get Matching Xpath Count    ${DE_component_list}
    : FOR    ${i}    IN RANGE    1    ${component_count}+1
    \    Set Global Variable    ${i}
    \    ${component_name}    Get Text    xpath=(${DE_component_list})[${i}]
    \    #Run Keyword If    '${component_name}' == 'Employment 1 (Latest/Current)'    Employment Check Data Entry 1
    \    Run Keyword If    '${component_name}' == 'Highest 1' or '${component_name}' == '10th' or '${component_name}' == 'Highest'    Highest 1 Check Data Entry 1
    \    Run Keyword If    '${component_name}' == 'Highest 2' or '${component_name}' == '12th'    Highest 2 Check Data Entry 2
    \    Run Keyword If    '${component_name}' == 'Highest 3' or '${component_name}' == 'Under Graduate 1'    Highest 3 Check Data Entry 3
    \    Run Keyword If    '${component_name}' == 'Highest 4' or '${component_name}' == 'Under Graduate 2'    Highest 4 Check Data Entry 4
    \    Run Keyword If    '${component_name}' == 'Highest 5' or '${component_name}' == 'Post Graduate 1'    Highest 5 Check Data Entry 5
    \    Run Keyword If    '${component_name}' == 'Highest 6' or '${component_name}' == 'Post Graduate 2'    Highest 6 Check Data Entry 6
    \    Run Keyword If    '${component_name}' == 'Reference 1'    Reference 1 Check Data Entry
    \    Run Keyword If    '${component_name}' == 'Reference 2'    Reference 2 Check Data Entry
    \    Run Keyword If    '${component_name}' == 'Reference 3'    Reference 3 Check Data Entry
    \    Run Keyword If    '${component_name}' == 'Reference 4'    Reference 4 Check Data Entry
    \    Run Keyword If    '${component_name}' == 'Current Address'    Current Address Check Data Entry
    \    Log To Console    ${component_name}
    Element Should Be Enabled    ${DE_Save_Submit}
    Click Element    ${DE_Save_Submit}
    Wait Until Element Is Visible    ${Save_ok}
    Element Text Should Be    ${message_path}    Case Submitted successfully.
    Click Element    ${Save_ok}

DataEntry GetNext
    Wait Until Element Is Visible    ${DE_DB_GetNext}
    Click Element    ${DE_DB_GetNext}
    Case Availability

Employment Check Data Entry 1
    Run Keyword If    ${i} == 1    Click Element    xpath=(${DE_component_list})[${i}]
    Sleep    2s
    Employment Check Inputs

Highest 1 Check Data Entry 1
    Run Keyword If    ${i} == 1    Click Element    xpath=(${DE_component_list})[${i}]
    Sleep    2s
    Education Data Entry For 10th and Highest 1

Highest 2 Check Data Entry 2
    Run Keyword If    ${i} == 1    Click Element    xpath=(${DE_component_list})[${i}]
    Sleep    2s
    Education Data Entry For 12th and Highest 2

Highest 3 Check Data Entry 3
    Run Keyword If    ${i} == 1    Click Element    xpath=(${DE_component_list})[${i}]
    Sleep    2s
    Education Data Entry For UG1 and Highest 3

Highest 4 Check Data Entry 4
    Run Keyword If    ${i} == 1    Click Element    xpath=(${DE_component_list})[${i}]
    Sleep    2s
    Education Data Entry For UG2 and Highest 4

Highest 5 Check Data Entry 5
    Run Keyword If    ${i} == 1    Click Element    xpath=(${DE_component_list})[${i}]
    Sleep    2s
    Education Data Entry For PG1 and Highest 5

Highest 6 Check Data Entry 6
    Run Keyword If    ${i} == 1    Click Element    xpath=(${DE_component_list})[${i}]
    Sleep    2s
    Education Data Entry For PG2 and Highest 6

Reference 1 Check Data Entry
    Run Keyword If    ${i} == 1    Click Element    xpath=(${DE_component_list})[${i}]
    Sleep    2s
    Reference Type Employment

Reference 2 Check Data Entry
    Run Keyword If    ${i} == 1    Click Element    xpath=(${DE_component_list})[${i}]
    Sleep    2s
    Reference Type Professional

Reference 3 Check Data Entry
    Run Keyword If    ${i} == 1    Click Element    xpath=(${DE_component_list})[${i}]
    Sleep    2s
    Reference Personal

Reference 4 Check Data Entry
    Run Keyword If    ${i} == 1    Click Element    xpath=(${DE_component_list})[${i}]
    Sleep    2s
    Reference Type Academic

Current Address Check Data Entry
    Run Keyword If    ${i} == 1    Click Element    xpath=(${DE_component_list})[${i}]
    Sleep    2s
    Current Address Check

Case Availability
    Sleep    3s
    ${refno}    Get Text    ${DE_Ref_Check}
    Run Keyword If    '${refno1}' == '${refno}' or '${refno2}' == '${refno}'    Do Data Entry
    Run Keyword If    ${countvalue} != ${casecount}-1    Run Keyword If    '${refno1}' != '${refno}' and '${refno2}' != '${refno}'    Next Case
    Run Keyword If    ${countvalue} == ${casecount}-1    Close Browser
    #or '${refno3}' == '${refno}'
    #and '${refno3}' != '${refno}'

Next Case
    Click Element    ${DE_Dashboard}
    Wait Until Element Is Visible    ${DE_DB_GetNext}
    DataEntry GetNext

Get Next Case with High Priority
    Log To Console    Checking for Normal Priority unreserved case
    ${getname} =    Get Text    xpath=(${VER_Refno})[${i}]
    Close Browser
    Service Provider Login 2 - portal
    Wait Until Element Is Visible    ${DE_Stage}
    Click Element    ${DE_Stage}
    Wait Until Element Is Visible    ${DE_DB_GetNext}
    Click Element    ${DE_DB_GetNext}
    Sleep    3s
    ${result}=    Run Keyword And Return Status    Page Should Contain    ${getname}
    Run Keyword if    ${result} == False    Fail
    Log To Console    Get Next Functionality working fine for unreserved case with High Priority
    Close Browser
    Service Provider Login - portal
    Click Element    ${dataentrysupervision_path}
    Sleep    2s
    #Log To Console    High Case

Get Next Case with Normal Priority
    Log To Console    Checking for Normal Priority unreserved case
    ${getname} =    Get Text    xpath=(${VER_Refno})[${i}]
    Wait Until Element Is Visible    ${DE_Stage}
    Click Element    ${DE_Stage}
    Wait Until Element Is Visible    ${DE_DB_GetNext}
    Click Element    ${DE_DB_GetNext}
    Sleep    3s
    ${result}=    Run Keyword And Return Status    Page Should Contain    ${getname}
    Run Keyword if    ${result} == False    Fail
    Log To Console    Get Next Functionality working fine for unreserved case with Normal Priority
    Click Element    ${DE_Dashboard}
    Wait Until Element Is Visible    ${dataentrysupervision_path}
    Click Element    ${dataentrysupervision_path}
    Sleep    2s
    #Log To Console    Normal Case

New Case Registration
    New Case Registration Section
    ${getfirstname}=    Set Variable    getfirstName
    Set Test Variable    ${${getfirstname}${i}}    ${randomfirstname}
    Set Global Variable    ${${getfirstname}${i}}
    Close Browser

New Case Registration Section
    Close Browser
    Random String New
    Browser-Headless
    Openworld Portal
    #Service Provider Login - portal
    Wait Until Element Is Visible    ${caseregisterlink_path}
    Click Element    ${caseregisterlink_path}
    Sleep    2s
    Click Button    ${newcaseregn_path}
    Sleep    1s
    Click Element    ${clientselection_path}
    Input Text    ${clientsearch_path}    ${clientname}
    Press Key    ${clientsearch_path}    \\13
    Wait Until Element Is Visible    ${dataentrybyselection_path}
    Click Element    ${dataentrybyselection_path}
    Click Element    ${selectdataentrybysp_path}
    Input Text    ${firstname_path}    ${randomfirstname}
    Input Text    ${lastname_path}    ${randomlastname}
    Click Element    ${dob_path}
    Click Element    ${dobyear_path}
    Click Element    ${dobyear_path}
    Click Element    ${dobarrow_path}
    Click Element    ${dobyearselection_path}
    Click Element    ${dobmonthselection_path}
    Click Element    ${dobdateselection_path}
    Click Element    ${gender_path}
    Click Element    ${genderoption_path}
    Input Text    ${mailid_path}    ${randomfirstname}@testmail.com
    Input Text    ${fatherfirstname_path}    ${fathersfirstname}
    Input Text    ${fatherlastname_path}    ${fatherslastname}
    Click Element    ${maritalstatus_path}
    Click Element    ${maritalstatusoption_path}
    Input Text    ${candidateid_path}    ${candidateidnew}
    Input Text    ${employeeid_path}    ${employeeidnew}
    Input Text    ${landlinenumber_path}    ${landlinenumber}
    Input Text    ${mobilenumber_path}    ${mobilenumber}
    Click Element    ${priority_path}
    Click Element    ${priorityoption_path}
    Click Element    ${selectallcomponent_path}
    Click Element    ${save_submit_path}
    Wait Until Element Is Visible    ${ok_path}
    Click Element    ${ok_path}

Logout
    Wait Until Element Is Visible    ${user_link}
    Click Element    ${user_link}
    Wait Until Element Is Visible    ${signout_button}
    Click Element    ${signout_button}
    Wait Until Element Is Visible    ${loginbutton}
    Element Should be visible    ${loginbutton}
