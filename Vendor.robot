*** key word ***
Checks select
    ${Check_Box}=    set variable    //div/input[@type='checkbox']
    ${Count}=    Get matching xpath count    ${Check_Box}
    Set Global Variable    ${Check_Box}
    #Log To Console    ${INDEX}
    #Log To Console    ${Count}
    click element    xpath=(${Check_Box})[${INDEX}]

Validate Checks
    ${Check_Box}=    set variable    //div/input[@type='checkbox']
    ${Count}=    Get matching xpath count    ${Check_Box}
    Set Global Variable    ${Check_Box}
    #Log To Console    ${INDEX}
    #Log To Console    ${Count}
    Checkbox Should Be Selected    xpath=(${Check_Box})[${INDEX}]

Create Vendor
    sleep    0.5s
    click element    ${Vendors}
    sleep    0.5s
    click element    ${Add_New_Vendor}
    sleep    0.5s
    click element    ${Vendor_Description}
    input text    ${Vendor_Description}    ${TEMP_FIRST_NAME}
    click element    ${Vendor_contactNo}
    input text    ${Vendor_contactNo}    ${RANDOM_NUMBER}
    sleep    2s
    click element    //input[@id='faxNo']/following-sibling::div/input
    input text    //input[@id='faxNo']/following-sibling::div/input    ${RANDOM_NUMBER}
    click element    ${Vendor_Website}
    input text    ${Vendor_Website}    www.${TEMP_FIRST_NAME}.com
    sleep    2s
    click element    ${Vendor_Name}
    input text    ${Vendor_Name}    ${TEMP_FIRST_NAME}
    sleep    3s
    click element    ${Vendor_pincode}
    input text    ${Vendor_pincode}    606604
    sleep    0.5s
    Press key    ${Vendor_pincode}    \\13
    sleep    2s
    click element    ${Vendor_Address}
    input text    ${Vendor_Address}    123,${TEMP_FIRST_NAME},chennai 606604
    sleep    2s

Create Vendor user
    click element    ${Vendor_Add_User}
    sleep    2s
    click element    ${Vendor_Firstname}
    input text    ${Vendor_Firstname}    ${TEMP_FIRST_NAME}
    click element    ${Vendor_Lastname}
    input text    ${Vendor_Lastname}    ${TEMP_LAST_NAME}
    click element    ${Vendor_Designation}
    input text    ${Vendor_Designation}    ${TEMP_FIRST_NAME}
    click element    ${Vendor_Email}
    input text    ${Vendor_Email}    ${TEMP_FIRST_NAME}@gmail.com
    click element    ${Vendor_Phoneno}
    input text    ${Vendor_Phoneno}    ${RANDOM_NUMBER}
    click element    ${Vendor_Faxno}
    input text    ${Vendor_Faxno}    ${RANDOM_NUMBER}
    click element    ${Vendor_Username}
    input text    ${Vendor_Username}    ${TEMP_FIRST_NAME}
    click element    ${Vendor_password}
    input text    ${Vendor_password}    ${CONFIRM_PASSWORD}
    click element    ${Vendor_con_password}
    input text    ${Vendor_con_password}    ${CONFIRM_PASSWORD}
    sleep    3s
    click element    ${Client_Project}
    click element    ${Vendor_county_input}
    input text    ${Vendor_county_input}    india
    click element    ${Vendor_county_slect}
    sleep    2s
    click element    ${Vendor_User_Pincode}
    input text    ${Vendor_User_Pincode}    606604
    sleep    0.5s
    Press key    ${Vendor_User_Pincode}    \\13
    sleep    3s
    click element    ${Vendor_User_Address}
    input text    ${Vendor_User_Address}    123,${TEMP_FIRST_NAME},chennai 606604
    sleep    2s
    click element    ${Vendor_User_Add}
    sleep    2s
    ${Count}=    Get matching xpath count    ${Vendor_checks_box}
    Set Global Variable    ${Vendor_checks_box}
    log to console    ${Count}
    sleep    2s
    click element    ${Vendor_Create}
    sleep    5s

Select the Checks
    sleep    3s
    ${Count}=    Get matching xpath count    ${Vendor_checks}
    Set Global Variable    ${Vendor_checks}
    @{Check_List} =    Create List    Education    Reference    Address    Criminal    Employment
    ...    ID    Database
    Set Global Variable    ${Check_List}
    ${Check_count}    Get Length    ${Check_List}
    Log To Console    ${Check_count}
    Set Global Variable    ${Check_count}
    : FOR    ${INDEX}    IN RANGE    1    ${Count}+1
    \    Set Global Variable    ${INDEX}
    \    ${ID_s}=    Get Text    xpath=(${Vendor_checks})[${INDEX}]
    \    Run Keyword If    '${ID_s}' == ${Check_List}[${INDEX}-1]    Checks select

Validate Selected Checks
    sleep    3s
    ${Count}=    Get matching xpath count    ${Vendor_checks}
    Set Global Variable    ${Vendor_checks}
    @{Check_List} =    Create List    Education    Reference    Address    Criminal    Employment
    ...    ID    Database
    Set Global Variable    ${Check_List}
    ${Check_count}    Get Length    ${Check_List}
    Log To Console    ${Check_count}
    Set Global Variable    ${Check_count}
    : FOR    ${INDEX}    IN RANGE    1    ${Count}+1
    \    Set Global Variable    ${INDEX}
    \    ${ID_s}=    Get Text    xpath=(${Vendor_checks})[${INDEX}]
    \    Run Keyword If    '${ID_s}' == ${Check_List}[${INDEX}-1]    Validate Checks
