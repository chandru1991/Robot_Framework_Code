newcaseregn_path="//button[text()=' Register New Case']"
clientselection_path="//div[@class='chzn-container chzn-container-single']"
clientsearch_path="//input[@type='text']"
selectclient_path="//ul[@class='chzn-results']"
dataentrybyselection_path="//select[@id='dataentryBy']"
selectdataentrybysp_path="//option[text()='Service Provider']"
selectdataentrybycandidate_path="//option[text()='Candidate']"
category_path='id=employmentStatus'
category_fresher_path="//option[text()='Fresher']"
category_exp_path="//option[text()='Experienced']"
firstname_path='id=fname'
lastname_path='id=lname'
dob_path='id=dob'
dobyear_path="//strong[@class='ng-binding']"
dobarrow_path="//button[@class='btn btn-default btn-sm pull-left']"
dobyearselection_path="//span[text()='1987']"
dobmonthselection_path="//span[text()='August']"
dobdateselection_path="//span[text()='11']"
gender_path='id=gender'
genderoption_path="//option[text()='Male']"
mailid_path='id=email'
fatherfirstname_path='id=fathersfname'
fatherlastname_path='id=faltherslname'
maritalstatus_path='id=maritalstatus'
maritalstatusoption_path="//option[text()='Married']"
candidateid_path='id=clientidtype'
employeeid_path='id=employeId'
landlinenumber_path="//input[@id='landLineNumber']/following-sibling::div/input"
mobilenumber_path="//input[@id='contactnum']/following-sibling::div/input"
priority_path='id=priorty'
priorityoption_path="//option[text()='High']"
hremailid_path="//input[@id='hrEmail']/following-sibling::div/input"
selectallcomponent_path="//input[@ng-model='selectAll']"
save_submit_path='id=submithide'
save_path="//button[text()='Save']"
ok_path="//button[text()='OK']"
firstnamefilter_path="//label[text()='First Name']/following-sibling::input"
lastnamefilter_path="//label[text()='Last Name']/following-sibling::input"
clientfilter_path="//li[@class='search-field']/input"
clientfilterclose_path="//a[@class='search-choice-close']"
filtersearch_path="//button[@id='searchBtn']"
click_case_path="//td[@class='sorting_1']"
message_path="//div[@class='message']"
yes_path="//button[text()='Yes']"
no_path="//button[text()='No']"
filterclear_path='id=clrBtn'
checkbox_selection_path="//input[@ng-model='compo.selected']"
comp_name_path="//*[@id='table']/tbody/tr/td/label/strong"
databasecomponent_path="//input[@class='ivh-treeview-checkbox ng-pristine ng-valid']"
databaseselectall_path="//input[@ng-model='dbselectAll']"
client_mandatorystar="//label[text()='Client/Project']/em"
client_mandatorysymbol="//div[@id='client_chzn']/following-sibling::span"
dataentryby_mandatorystar="//label[text()='Please select who is supposed to do data entry']/em"
dataentryby_mandatorysymbol="//select[@id='dataentryBy']/following-sibling::span"
firstname_mandatorystar="//label[text()='First Name']/em"
firstname_mandatorysymbol="//input[@id='fname']/following-sibling::span"
lastname_mandatorystar="//label[text()='Last Name']/em"
lastname_mandatorysymbol="//input[@id='lname']/following-sibling::span"
dob_mandatorystar="//label[text()='Date of Birth']/em"
dob_mandatorysymbol="//input[@id='dob']/following-sibling::span"
gender_mandatorystar="//label[text()='Gender']/em"
gender_mandatorysymbol="//select[@id='gender']/following-sibling::span"
email_mandaorystar="//label[text()='Email']/em"
email_mandatorysymbol="//input[@id='email']/following-sibling::span"
nationality_path='id=nationality'
fatherfname_mandatorysymbol="//input[@id='fathersfname']/following-sibling::span"
fatherlname_mandatorysymbol="//input[@id='faltherslname']/following-sibling::span"
maritalstatus_mandatorysymbol="//select[@id='maritalstatus']/following-sibling::span"
maritalstatus_mandatorystar="//label[text()='Marital Status ']/em"
candidateid_mandatorystar="//label[text()='Candidate Id']/em"
candidateid_mandatorysymbol="//input[@id='clientidtype']/following-sibling::span"
employeeid_mandatorystar="//label[text()='Employee Id']/em"
employeeid_mandatorysymbol="//input[@id='employeId']/following-sibling::span"
nationality_mandatorystar="//label[contains(text(),'Nationality')]/em"
nationality_mandatorysymbol="//input[@id='nationality']/following-sibling::span"
