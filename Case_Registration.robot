*** Keywords ***
Compounds Selection
    #@{values}=    Create List    Highest 1    Highest 2    Reference 1    Employment 1(Latest/Current)    Current Address
    #...    Permanent Address    Database
    #${count_values}=    Get Length    ${values}
    #Set Global Variable    ${count_values}
    ${Componentxpath}=    set variable    //*[@id='table']/tbody/tr/td/label/strong
    ${Count}=    Get matching xpath count    ${Componentxpath}
    Set Global Variable    ${Componentxpath}
    #Log To Console    ${Count}
    : FOR    ${INDEX}    IN RANGE    1    ${Count}+1
    \    #Log To Console    ${INDEX}
    \    Set Global Variable    ${INDEX}
    \    ${components}=    Get Text    xpath=(${Componentxpath})[${INDEX}]
    \    Set Global Variable    ${components}
    \    #Log To Console    INDEX:${INDEX}    COPONENTS:${components}
    \    Run Keyword If    '${components}' == 'Highest 1'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Highest 2'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == '10th'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == '12th'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Diploma/ITI'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Under Graduate 1'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Under Graduate 2'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Post Graduate 1'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Post Graduate 2'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Professional 1'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Professional 2'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Reference 1'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Reference 2'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Reference 3'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Reference 4'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Reference 5'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Employment 1(Latest/Current)'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Employment 2(Latest/Current)'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Employment 3(Latest/Current)'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Current Address'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Permanent Address'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Address 1'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Address 2'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Address 3'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Address 4'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Address 5'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Aadhaar Card'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Driving Licence'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Voters ID'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'National Insurance Card'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'National ID Card'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'PAN Card'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Passport'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Ration card'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Social Security Number'    Run Keyword    Check BOX
    \    #Run Keyword If    '${components}' == 'Social Insurance Number'    Run Keyword    Check BOX
    \    Run Keyword If    '${components}' == 'Database'    Run Keyword    Check BOX

Check BOX
    ${Check_Box}=    set variable    //input[@ng-model='compo.selected']
    ${Count}=    Get matching xpath count    ${Check_Box}
    Set Global Variable    ${Check_Box}
    #Log To Console    ${INDEX}
    #Log To Console    ${Count}
    click element    xpath=(${Check_Box})[${INDEX}]

Case registeration Dash board in Client flow
    click element    ${Case_Registration}
    sleep    2s
    wait until element is enabled    ${DB_Client}
    click element    ${DB_Client}
    input text    ${DB_Cli_Text}    ${client1}
    click element    ${DB_Cli_Selc}
    wait until element is enabled    ${DB_Firstname}
    input text    ${DB_Firstname}    ${FName}
    wait until element is enabled    ${DB_Lastname}
    input text    ${DB_Lastname}    ${LName}
    sleep    3s
    click element    ${Search_Button}
    sleep    3s
    ${REF_NO}=    Get Text    ${DB_REFNO}
    Log To Console    ${REF_NO}

Case registeration Dash board in Project flow
    click element    ${Case_Registration}
    sleep    2s
    wait until element is enabled    ${DB_Client}
    click element    ${DB_Client}
    input text    ${DB_Cli_Text}    ${Uni_Project1}
    click element    ${DB_Cli_Selc}
    wait until element is enabled    ${DB_Firstname}
    input text    ${DB_Firstname}    ${FName}
    wait until element is enabled    ${DB_Lastname}
    input text    ${DB_Lastname}    ${LName}
    sleep    3s
    click element    ${Search_Button}
    sleep    3s
    ${REF_NO}=    Get Text    ${DB_REFNO}
    Log To Console    ${REF_NO}

Candiate Client flow with Temprory save
    sleep    3s
    click element    ${Case_Registration}
    sleep    2s
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Client1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Candidate}
    click element    ${Candidate}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}
    sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    sleep    2s
    click element    ${Save}
    sleep    2s
    click element    ${Save_ok}
    sleep    2s

Service Provider Client flow with Temprory save
    sleep    3s
    click element    ${Case_Registration}
    sleep    2s
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Client1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Service_Provider}
    click element    ${Service_Provider}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}
    sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    sleep    2s
    click element    ${Save}
    sleep    2s
    click element    ${Save_ok}
    sleep    2s

Candiate Client flow with Temprory save & submit
    sleep    3s
    click element    ${Case_Registration}
    sleep    2s
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Client1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Candidate}
    click element    ${Candidate}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}
    sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    sleep    2s
    click element    ${Save_Submit}
    sleep    2s

Service Provider Client flow with Temprory save & submit
    sleep    3s
    click element    ${Case_Registration}
    sleep    2s
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Client1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Service_Provider}
    click element    ${Service_Provider}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}
    sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    sleep    2s
    click element    ${Save_Submit}
    sleep    2s
    #project

Candiate Project flow with Temprory save
    sleep    3s
    click element    ${Case_Registration}
    sleep    2s
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Uni_Project1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Candidate}
    click element    ${Candidate}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}
    sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    sleep    2s
    click element    ${Save}
    sleep    2s
    click element    ${Save_ok}
    sleep    2s

Service Provider Project flow with Temprory save
    sleep    3s
    click element    ${Case_Registration}
    sleep    2s
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Uni_Project1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Service_Provider}
    click element    ${Service_Provider}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}
    sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    sleep    2s
    click element    ${Save}
    sleep    2s
    click element    ${Save_ok}
    sleep    2s

Candiate Project flow with Temprory save & submit
    sleep    3s
    click element    ${Case_Registration}
    sleep    2s
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Uni_Project1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Candidate}
    click element    ${Candidate}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}
    sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    sleep    2s
    click element    ${Save_Submit}
    sleep    2s

Service Provider Project flow with Temprory save & submit
    sleep    3s
    click element    ${Case_Registration}
    sleep    2s
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Uni_Project1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Service_Provider}
    click element    ${Service_Provider}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}
    sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    sleep    2s
    click element    ${Save_Submit}
    sleep    2s

After set password validate the password field and user login
    sleep    2s
    input text    ${NEW_PASSWORD LOCATION}    ${INVALID}
    click element    ${CONFIRM BUTTON}
    sleep    3s
    click element    ${PASSWORD_ALERT}
    input text    ${CONFIRM PASSWORD LOCATION}    ${INVALID}
    sleep    3s
    click element    ${CONFIRM BUTTON}
    sleep    3s
    click element    ${PASSWORD_ALERT}
    Clear Element Text    ${NEW_PASSWORD LOCATION}
    input text    ${NEW_PASSWORD LOCATION}    ${Email}
    Clear Element Text    ${CONFIRM PASSWORD LOCATION}
    input text    ${CONFIRM PASSWORD LOCATION}    ${INVALID}
    sleep    3s
    click element    ${CONFIRM BUTTON}
    sleep    3s
    click element    ${PASSWORD_ALERT}
    input text    ${NEW_PASSWORD LOCATION}    ${INVALID}
    input text    ${CONFIRM PASSWORD LOCATION}    ${CONFIRM PASSWORD}
    sleep    3s
    click element    ${CONFIRM BUTTON}
    sleep    3s
    click element    ${PASSWORD_ALERT}
    Clear Element Text    ${NEW_PASSWORD LOCATION}
    input text    ${NEW_PASSWORD LOCATION}    ${EMPTY}
    Clear Element Text    ${CONFIRM PASSWORD LOCATION}
    input text    ${CONFIRM PASSWORD LOCATION}    ${EMPTY}
    sleep    3s
    click element    ${CONFIRM BUTTON}
    sleep    3s
    click element    ${PASSWORD_ALERT}
    Clear Element Text    ${NEW_PASSWORD LOCATION}
    input text    ${NEW_PASSWORD LOCATION}    ${RANDOM_STRING}@1011
    Clear Element Text    ${CONFIRM PASSWORD LOCATION}
    input text    ${CONFIRM PASSWORD LOCATION}    ${RANDOM_STRING}@1011
    sleep    3s
    click element    ${CONFIRM BUTTON}
    sleep    3s
    click element    ${PASSWORD_ALERT}
    sleep    3s
    input text    ${USER LOCATION}    ${RANDOM_STRING}@gmail.com
    input password    ${PASSWORD LOCATION}    ${RANDOM_STRING}@1011
    click button    ${LOGIN}
    sleep    4s
    click element    ${User}
    sleep    3s
    click element    ${Sign_Out}

Setup user password in the corresponding user mail ID
    open Browser    ${G_URL}    ${BROWSER}
    sleep    3s
    Maximize Browser Window
    sleep    5s
    input text    ${GMAIL_LOCATION}    ${GMAIL_ID}
    click element    ${ID_NEXT}
    sleep    5s
    input text    ${GMAIL_PASSWORD_LOCATION}    ${GMAIL_PASSWORD}
    click element    ${PASSWORD_NEXT}
    sleep    10s
    click element    ${E_SEARCH}
    input text    ${E_SEARCH}    ${Gmail_Search}
    sleep    5s
    click element    ${E_S_CLICK}
    sleep    5s
    click element    ${E_IN_LINK}
    sleep    3s
    ${D_count}=    Get matching xpath count    ${Dot}
    Log To Console    ${D_count}
    sleep    3s
    ${D_count_link}=    Get matching xpath count    ${Setup_Password}
    Log To Console    ${D_count_link}
    sleep    3s
    Run Keyword If    ${D_count}>0    click element    xpath=(${Dot})[${D_count}]
    sleep    3s
    click element    xpath=(${Setup_Password})[${D_count_link}]

After approve client only data move to service provider-client
    sleep    3s
    click element    ${Case_Registration}
    sleep    2s
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Client1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Candidate}
    click element    ${Candidate}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}
    sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    sleep    2s
    click element    ${Client_Approval_Yes}
    sleep    1s
    click element    ${Save_Submit}
    sleep    2s
    Click Element    ${Save_ok}
    sleep    2s

After approve client only data move to service provider-Project
    sleep    3s
    click element    ${Case_Registration}
    sleep    2s
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Uni_Project1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Candidate}
    click element    ${Candidate}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}
    sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    sleep    2s
    click element    ${Client_Approval_Yes}
    sleep    1s
    click element    ${Save_Submit}
    sleep    2s
    Click Element    ${Save_ok}
    sleep    2s

After approve client only data move to service provider save -client
    sleep    3s
    click element    ${Case_Registration}
    sleep    2s
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Client1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Candidate}
    click element    ${Candidate}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}
    sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    sleep    2s
    click element    ${Client_Approval_Yes}
    sleep    1s
    click element    ${Save}
    sleep    2s
    Click Element    ${Save_ok}
    sleep    2s

After approve client only data move to service provider save -Project
    sleep    3s
    click element    ${Case_Registration}
    sleep    2s
    click element    ${Register_New_Case}
    wait until element is enabled    ${Client_Project}
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Uni_Project1}
    click element    ${Client_Select}
    wait until element is enabled    ${Data_Entry}
    click element    ${Data_Entry}
    wait until element is enabled    ${Candidate}
    click element    ${Candidate}
    wait until element is enabled    ${First_Name_Location}
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}
    wait until element is enabled    ${Email_Location}
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}
    sleep    3s
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}
    click element    ${Priority}
    click element    ${Priority_Select}
    click element    ${checkBOx}
    sleep    2s
    click element    ${Client_Approval_Yes}
    sleep    1s
    click element    ${Save}
    sleep    2s
    Click Element    ${Save_ok}
    sleep    2s
