*** Keywords ***
Database
    #Login
    #Random String
    #Random String Search values
    #sleep    2s
    #click element    ${Verification_Supervision}
    sleep    3s
    click element    ${VERS_DB_ClientSearch}
    input text    ${VERS_DB_ClientSearch}    ${Client1}
    sleep    2s
    Press Key    ${VERS_DB_ClientSearch}    \\13
    click element    ${VERS_DB_FirstName}
    input text    ${VERS_DB_FirstName}    ${FName}
    click element    ${VERS_DB_LastName}
    input text    ${VERS_DB_LastName}    ${LName}
    sleep    2s
    click element    ${DE_Search_Button}
    sleep    3s
    ${Componentxpath}=    set variable    //*[@role="row"]/td[13]
    ${Count}=    Get matching xpath count    ${Componentxpath}
    Set Global Variable    ${Componentxpath}
    : FOR    ${INDEX}    IN RANGE    1    ${Count}+1
    \    Set Global Variable    ${INDEX}
    \    ${components}=    Get Text    xpath=(${Componentxpath})[${INDEX}]
    \    sleep    3s
    \    log to console    xpath=(${Componentxpath})[${INDEX}]
    \    Run Keyword If    '${components}' == 'Database'    DataBase Select

DataBase Select
    sleep    3s
    click element    ${VER_DataBaseCaseSelc}
    sleep    2s
    sleep    2s
    click element    ${VER_DataBase_Outcome}
    sleep    2s
    click element    ${VER_DataBase_NoRec}
    sleep    2s
    click element    ${VER_Submit}
    sleep    2s
    click element    ${Save_ok}

Reserve
    ${REF_NO}=    Set Variable    001002363
    ${FName}=    Set Variable    Fred
    ${LName}=    Set Variable    Curry
    click element    ${VERS_DB_ClientSearch}
    input text    ${VERS_DB_ClientSearch}    ${Client1}
    sleep    2s
    Press Key    ${VERS_DB_ClientSearch}    \\13
    click element    ${VERS_DB_FirstName}
    input text    ${VERS_DB_FirstName}    ${FName}
    click element    ${VERS_DB_LastName}
    input text    ${VERS_DB_LastName}    ${LName}
    sleep    2s
    click element    ${DE_Search_Button}
    sleep    3s
    ${Componentxpath}=    set variable    ${DE_DB_Reserved}
    ${Count}=    Get matching xpath count    ${Componentxpath}
    Set Global Variable    ${Componentxpath}
    : FOR    ${INDEX}    IN RANGE    1    ${Count}+1
    \    Set Global Variable    ${INDEX}
    \    click element    xpath=(${Componentxpath})[${INDEX}]
    \    sleep    3s
    \    click element    xpath=(${DE_DB_ResUser})[${INDEX}]
    \    sleep    4s
