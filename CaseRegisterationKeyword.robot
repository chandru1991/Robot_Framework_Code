*** key words ***
Select Case Registeration
    click element    ${Case_Registration}

Select New Case Registeration
    click element    ${Register_New_Case}

Select Client for Assign Case
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Client1}
    click element    ${Client_Select}

Select Project for Assign Case
    click element    ${Client_Project}
    input text    ${Client_Location}    ${Uni_Project1}
    click element    ${Client_Select}

Select Who Supposed to do Data Entry
    click element    ${Data_Entry}
    click element    ${Candidate}

Enter the Candidate First and the Second Name
    input text    ${First_Name_Location}    ${FName}
    input text    ${Last_Name_Location}    ${LName}

Select the Date of Birth
    click element    ${Date_Of_Birth}
    click element    ${Month}
    click element    ${Years}
    click element    ${Year}
    click element    ${Select_Year}
    click element    ${Select_Month}
    click element    ${Select_Day}
    click element    ${Gender_Location}
    click element    ${Gender_Select}

Enter the Candidate Mail Id
    input text    ${Email_Location}    ${RANDOM_STRING}@gmail.com

Enter the Candidate Father First and Last Name
    input text    ${Father_First_Name_Location}    ${LName}
    input text    ${Father_Last_Name_Location}    ${FName}

Enter the Client Reference Number
    click element    ${Client_ID}
    input text    ${Client_ID}    ${Client_Ref_NO}

Select the Marital Status
    click element    ${Marital_Status}
    click element    ${Marital_Status_Select}

Enter the Candidate Employee ID
    click element    ${Employe_ID_Location}
    input text    ${Employe_ID_Location}    ${Employe_ID}

Enter the Candidate LandLine and Mobile Number
    input text    ${Landline_Number_Location}    ${RANDOM_NUMBER}
    input text    ${Mobile_Number_Location}    ${RANDOM_NUMBER}

Select the Candidate Priority
    click element    ${Priority}
    click element    ${Priority_Select}

With Client Approval Case
    click element    ${Client_Approval_Yes}

Without Client APproval Case
    click element    ${Client_Approval_No}

With Client Approval Radio Button Validation
    Element Should Be visible    ${Client_Approval_Val}

Without Client Approval Radio Button Validation
    Element Should Be visible    ${Client_Approval_no_Val}

Select the all the check in the client
    click element    ${checkBOx}

Save and Submit the Candidate Details
    click element    ${Save_Submit}

Temprory save the candidate Details
    click element    ${Save}

Click on the OK button is the Alert
    click element    ${Save_ok}
