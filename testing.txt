*** settings ***
Library           Selenium2Library
Variables         clientListVariables.py
Library           rand_vals.py

*** variables ***
${name}           name

*** Test Cases ***
tc_1_open_browser
    open browser    ${url}    ${browser}

tc_open_browser
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    headless
    Call Method    ${chrome_options}    add_argument    disable-gpu
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size    1920    1080
    Go To    ${URL}

tc_2_login
    click element    ${uid_loc}
    input text    ${uid_loc}    ${uid}
    click element    ${pswd_loc}
    input text    ${pswd_loc}    ${pswd}
    click element    ${login_btn_loc}

tc_3_clients_list
    ${name}=    rand_name
    ${code}=    rand_cli_code
    Log to console    name: ${name}
    Log to console    client code:    ${cli_code}
    wait until element is visible    xpath=(${clients_list})[1]    100s
    click element    xpath=(${clients_list})[1]
    wait until element is visible    xpath=(${clients_list})[2]    100s
    click element    xpath=(${clients_list})[2]
    wait until element is visible    ${add_new_client}    100s
    click element    ${add_new_client}
    wait until element is visible    ${cli_name}    100s
    click element    ${cli_name}
    input text    ${cli_name}    ${name}
    click element    xpath=(${num})[1]
    input text    xpath=(${num})[1]    9876543210
    press key    xpath=(${num})[1]    \\13
    sleep    2s
    click element    ${service_provider_branch}
    sleep    2s
    Select From List By Value    ${service_provider_branch}    3
    click element    ${pincode}
    input text    ${pincode}    600001
    sleep    2s
    press key    ${pincode}    \\13
    sleep    2s
    click element    ${addr}
    input text    ${addr}    rlevgki,okrnw32cf dfsv,213,rfegv-21
    click element    ${cli_code}
    input text    ${cli_code}    ${code}
    click element    ${cli_url}
    input text    ${cli_url}    hi_hello
    click element    ${add_user}
    wait until element is visible    ${fname}    100s
    click element    ${fname}
    input text    ${fname}    ${name}
    click element    ${lname}
    input text    ${lname}    ${name}
    click element    ${email}
    input text    ${email}    tryingagain@me.com
    click element    ${user}
    input text    ${user}    ${name}
    click element    ${password}
    input text    ${password}    me@1234
    click element    ${confirm}
    input text    ${confirm}    me@1234
    click element    ${u_num}
    input text    ${u_num}    9875643210
    click element    ${country}
    input text    ${in_con}    india
    #Select from list by value    ${country}    india
    press key    ${in_con}    \\13
    click element    ${u_pin}
    input text    ${u_pin}    600009
    sleep    2s
    press key    ${u_pin}    \\13
    sleep    2s
    click element    ${add}
    wait until element is visible    ${create}    100s
    sleep    2s
    click element    ${create}
    sleep    2s
    click element    ${alert}

tc_close_browser
    close browser
