*** Keywords ***
Payment Selection
    ${Componentxpath}=    set variable    ${INS_PaymentModeTxt}
    ${Count}=    Get matching xpath count    ${Componentxpath}
    Set Global Variable    ${Componentxpath}
    #Log To Console    ${Count}
    : FOR    ${INDEX}    IN RANGE    1    ${Count}+1
    \    #Log To Console    ${INDEX}
    \    Set Global Variable    ${INDEX}
    \    ${components}=    Get Text    xpath=(${Componentxpath})[${INDEX}]
    \    Set Global Variable    ${components}
    \    #Log To Console    INDEX:${INDEX}    COPONENTS:${components}
    \    Run Keyword If    '${components}' == 'Cash'    Run Keyword    Check Payment
    \    Run Keyword If    '${components}' == 'Cheque'    Run Keyword    Check Payment
    \    Run Keyword If    '${components}' == 'Demand Draft'    Run Keyword    Check Payment
    \    Run Keyword If    '${components}' == 'Bank Transfer'    Run Keyword    Check Payment

Check Payment
    ${Check_Box}=    set variable    ${INS_CheckBoxCount}
    ${Count}=    Get matching xpath count    ${Check_Box}
    Set Global Variable    ${Check_Box}
    #Log To Console    ${INDEX}
    #Log To Console    ${Count}
    click element    xpath=(${Check_Box})[${INDEX}]

Emp Mail Selection
    ${Componentxpath}=    set variable    ${EMP_EmailText}
    ${Count}=    Get matching xpath count    ${Componentxpath}
    Set Global Variable    ${Componentxpath}
    #Log To Console    ${Count}
    : FOR    ${INDEX}    IN RANGE    1    ${Count}+1
    \    #Log To Console    ${INDEX}
    \    Set Global Variable    ${INDEX}
    \    ${components}=    Get Text    xpath=(${Componentxpath})[${INDEX}]
    \    Set Global Variable    ${components}
    \    #Log To Console    INDEX:${INDEX}    COPONENTS:${components}
    \    Run Keyword If    '${components}' == 'Standard Template'    Run Keyword    Check Email Tmp
    \    Run Keyword If    '${components}' == 'sample Template'    Run Keyword    Check Email Tmp
    \    Run Keyword If    '${components}' == 'New Template'    Run Keyword    Check Email Tmp
    \    Run Keyword If    '${components}' == 'Template Check'    Run Keyword    Check Email Tmp
    \    Run Keyword If    '${components}' == 'New Temp'    Run Keyword    Check Email Tmp
    \    Run Keyword If    '${components}' == 'Welcome mail'    Run Keyword    Check Email Tmp
    \    Run Keyword If    '${components}' == 'Standard Template(Single Candidate)'    Run Keyword    Check Email Tmp

Check Email Tmp
    ${Check_Box}=    set variable    ${EMP_EmailCheck}
    ${Count}=    Get matching xpath count    ${Check_Box}
    Set Global Variable    ${Check_Box}
    #Log To Console    ${INDEX}
    #Log To Console    ${Count}
    click element    xpath=(${Check_Box})[${INDEX}]

Document Selection
    ${Componentxpath}=    set variable    ${EMP_DocumentText}
    ${Count}=    Get matching xpath count    ${Componentxpath}
    Set Global Variable    ${Componentxpath}
    #Log To Console    ${Count}
    : FOR    ${INDEX}    IN RANGE    1    ${Count}+1
    \    #Log To Console    ${INDEX}
    \    Set Global Variable    ${INDEX}
    \    ${components}=    Get Text    xpath=(${Componentxpath})[${INDEX}]
    \    Set Global Variable    ${components}
    \    #Log To Console    INDEX:${INDEX}    COPONENTS:${components}
    \    Run Keyword If    '${components}' == 'Offer Letter'    Run Keyword    Check Document
    \    Run Keyword If    '${components}' == 'Service Letter'    Run Keyword    Check Document
    \    Run Keyword If    '${components}' == 'Payslip'    Run Keyword    Check Document
    \    Run Keyword If    '${components}' == 'Relieving Letter'    Run Keyword    Check Document

Check Document
    ${Check_Box}=    set variable    ${EMP_DocumentCheck}
    ${Count}=    Get matching xpath count    ${Check_Box}
    Set Global Variable    ${Check_Box}
    #Log To Console    ${INDEX}
    #Log To Console    ${Count}
    click element    xpath=(${Check_Box})[${INDEX}]
