*** keyword ***
dataentry
    ${comp1}=    'Highest 1'
    ${comp2}=    'Reference 1'
    ${comp3}=    'Database'
    @{Count}    ${comp1}    ${comp2}    ${comp3}
    Set Global Variable    @{Count}

click checks
    click element    xpath=(${xpath})[${i}]

DataEntry DashBoard
    click element    ${DataEntrySupervision}
    sleep    3s
    click element    ${DE_DB_Client}
    input text    ${DE_DB_Cli_Text}    ${Client1}
    click element    ${DE_DB_Cli_Selc}
    input text    ${DE_Firstname_Location}    ${FIRST_NAME}
    input text    ${DE_Lastname_Location}    ${LAST_NAME}
    sleep    3s
    Page Should Contain    ${Client1}
    Page Should Contain    ${FIRST_NAME}
    Page Should Contain    ${LAST_NAME}
    click element    ${Search_Button}

Employement Check
    sleep    3s
    click element    //span[text()='Data Entry']
    sleep    3s
    click element    ${DE_DB_Client}
    input text    ${DE_DB_Cli_Text}    Universal
    click element    ${DE_DB_Cli_Selc}
    input text    ${DE_Firstname_Location}    Lpgug
    input text    ${DE_Lastname_Location}    V
    click element    ${Search_Button}
    sleep    3s
    ${REF_NO}=    Get Text    ${DB_REFNO}
    Log To Console    ${REF_NO}
    click element    ${DB_REFNO}
    sleep    3s
    click element    //*[@id='Employment_1']/div[1]/a
    sleep    2s
    click element    ${DE_Emp_Company}
    sleep    2s
    Employment Check Inputs

Employment Check Inputs
    Input Text    ${DE_Emp_Company}    Kadamba Technologies
    Press Key    ${DE_Emp_Company}    \\13
    sleep    5s
    #click element    ${DE_Emp_sameas_Emp}
    #Sleep    5s
    Input Text    ${DE_EMP_Pincode}    600069
    Sleep    2s
    Press Key    ${DE_EMP_Pincode}    \\13
    sleep    3s
    Clear Element Text    ${DE_Emp_Company}
    Input Text    ${DE_Emp_Company}    Kadamba Technologies
    Press Key    ${DE_Emp_Company}    \\13
    sleep    5s
    Click Element    ${DE_Emp_fromDate}
    click element    ${Select_Day}
    click element    ${DE_Emp_TillDate}
    click element    ${DE_Emp_ID}
    input text    ${DE_Emp_ID}    KAD0007
    click element    ${DE_Emp_Designation}
    input text    ${DE_Emp_Designation}    Scrum Tester
    click element    ${DE_Emp_Department}
    input text    ${DE_Emp_Department}    Software Automation Tester
    click element    ${DE_Emp_LastCTC}
    input text    ${DE_Emp_LastCTC}    1500000
    click element    ${DE_Emp_Currency}
    click element    ${DE_Emp_Currency_INR}
    click element    ${DE_Emp_Emploiyee_period}
    click element    ${DE_Emp_Per_Annum}
    click element    ${DE_Emp_TypeOfEmployement}
    click element    ${DE_Emp_Permanent}
    sleep    2s
    : FOR    ${INDEX}    IN RANGE    1    3
    \    Set Global Variable    ${INDEX}
    \    log to console    ${INDEX}
    \    sleep    2s
    \    click element    //div[@id='person${INDEX}']/div[2]/div[1]/input
    \    input text    //div[@id='person${INDEX}']/div[2]/div[1]/input    The King
    \    sleep    2s
    \    click element    //div[@id='person${INDEX}']/div[2]/div[2]/input
    \    input text    //div[@id='person${INDEX}']/div[2]/div[2]/input    Scrum Tester
    \    sleep    2s
    \    click element    //div[@id='person${INDEX}']/div[2]/div[5]/input/following-sibling::div/input
    \    input text    //div[@id='person${INDEX}']/div[2]/div[5]/input/following-sibling::div/input    1234567890
    \    sleep    2s
    \    click element    //div[@id='person${INDEX}']/div[2]/div[6]/input/following-sibling::div/input
    \    input text    //div[@id='person${INDEX}']/div[2]/div[6]/input/following-sibling::div/input    King@kingdom.com
    \    sleep    2s
    \    click element    //div[@id='person${INDEX}']/div[2]/div[7]/input/following-sibling::div/input
    \    input text    //div[@id='person${INDEX}']/div[2]/div[7]/input/following-sibling::div/input    65849762
    sleep    2s
    click element    ${DE_Emp_ResonForLeaving}
    input text    ${DE_Emp_ResonForLeaving}    Location change
    click element    ${DE_Emp_Termination}
    input text    ${DE_Emp_Termination_Reson}    Due to some personal reson i'm not able to go office in peak time
    click element    ${DE_Emp_Comments}
    click element    ${DE_Emp_Comments}
    input text    ${DE_Emp_Comments}    nothing
    sleep    2s
    click element    ${DE_Save}

Education Check
    sleep    3s
    click element    //span[text()='Data Entry']
    sleep    3s
    click element    ${DE_DB_Client}
    input text    ${DE_DB_Cli_Text}    Universal
    click element    ${DE_DB_Cli_Selc}
    input text    ${DE_Firstname_Location}    Lpgug
    input text    ${DE_Lastname_Location}    V
    click element    ${Search_Button}
    sleep    3s
    ${REF_NO}=    Get Text    ${DB_REFNO}
    Log To Console    ${REF_NO}
    click element    ${DB_REFNO}
    sleep    4s
    click element    ${DE_Edc_High}
    sleep    2s
    click element    ${DE_Edc_10th}
    Education Check Inputs

Education Check Inputs
    ${Type_Status}=    Run Keyword and Return Status    Element Should Be Visible    ${DE_EDC_EduType}
    Run Keyword If    ${Type_Status} == True    Select from list by Label    ${DE_EDC_EduType}    10th
    click element    ${DE_Edc_InstituteName}
    input text    ${DE_Edc_InstituteName}    Vel Tech School
    Press Key    ${DE_Edc_InstituteName}    \\13
    click element    ${DE_Edc_NameOfCourse}
    input text    ${DE_Edc_NameOfCourse}    Higher Education
    click element    ${DE_Edc_MajorSubject}
    input text    ${DE_Edc_MajorSubject}    Maths, Computer Science
    sleep    2s
    click element    ${DE_Edc_TypeOfEducation}
    click element    ${DE_Edc_FullTime}
    click element    ${DE_Edc_CandidateName}
    input text    ${DE_Edc_CandidateName}    The King
    click element    ${DE_Edc_RegistrationNo}
    input text    ${DE_Edc_RegistrationNo}    512213205013
    click element    ${DE_Edc_CGPA}
    input text    ${DE_Edc_CGPA}    95%
    sleep    2s
    click element    ${DE_Edc_Coursestart}
    sleep    2s
    click element    ${Years}
    sleep    2s
    click element    ${Year}
    sleep    2s
    click element    ${Select_Month}
    sleep    2s
    click element    ${Select_Day}
    sleep    3s
    click element    ${DE_Edc_Courseend}
    sleep    2s
    click element    xpath=(//button[@class='btn btn-default btn-sm btn-block'])[2]
    click element    xpath=(//button[@ng-click='move(-1)'])[2]
    click element    //span[contains(text(),'May')]
    click element    xpath=(//span[contains(text(),'08')])[2]
    sleep    3s
    click element    ${DE_Edc_Gap}
    click element    ${DE_Edc_GapCmd}
    input text    ${DE_Edc_GapCmd}    Due to health issue 1 year i'm not go to school
    click element    ${DE_Edc_Comments}
    input text    ${DE_Edc_Comments}    none
    click element    ${DE_Save}
    Wait Until Element Is Visible    ${ok_path}
    Element Text Should Be    ${message_path}    Saved successfully
    Click Element    ${ok_path}

Reference Personal
    sleep    3s
    click element    ${DE_Ref_RefType}
    sleep    2s
    click element    ${DE_Ref_Personal}
    click element    ${DE_Ref_Name}
    input text    ${DE_Ref_Name}    the king
    click element    ${DE_Ref_Designation}
    input text    ${DE_Ref_Designation}    Scrum Tester
    click element    ${DE_Ref_Relationship}
    input text    ${DE_Ref_Relationship}    Friend
    click element    ${DE_Ref_ContactNo}
    input text    ${DE_Ref_ContactNo}    1234567890
    click element    ${DE_Ref_Email}
    input text    ${DE_Ref_Email}    king@kingdom.com
    sleep    2s
    click element    ${DE_Ref_Pincode}
    input text    ${DE_Ref_Pincode}    606604
    sleep    1s
    Press Key    ${DE_Ref_Pincode}    \\13
    sleep    2s
    click element    ${DE_Ref_Area}
    click element    ${DE_Ref_OtherArea}
    click element    ${DE_Ref_Other}
    input text    ${DE_Ref_Other}    ABC, 123 XYZ road
    click element    ${DE_Ref_AddressLine}
    input text    ${DE_Ref_AddressLine}    123,abc road.
    click element    ${DE_Save}
    sleep    1s
    click element    ${Save_ok}

Reference Acadamic
    sleep    3s
    click element    ${DE_Ref_Reference}
    sleep    2s
    Reference Type Academic

Reference Type Academic
    click element    ${DE_Ref_RefType}
    sleep    2s
    click element    ${DE_Ref_Academic}
    click element    ${DE_Ref_Name}
    input text    ${DE_Ref_Name}    the king
    click element    ${DE_Ref_Designation}
    input text    ${DE_Ref_Designation}    Scrum Tester
    click element    ${DE_Ref_Relationship}
    input text    ${DE_Ref_Relationship}    Friend
    click element    ${DE_Ref_ContactNo}
    input text    ${DE_Ref_ContactNo}    1234567890
    click element    ${DE_Ref_Email}
    input text    ${DE_Ref_Email}    king@kingdom.com
    sleep    2s
    click element    ${DE_Ref_Pincode}
    input text    ${DE_Ref_Pincode}    606604
    sleep    1s
    Press Key    ${DE_Ref_Pincode}    \\13
    sleep    2s
    click element    ${DE_Ref_Area}
    click element    ${DE_Ref_OtherArea}
    click element    ${DE_Ref_Other}
    input text    ${DE_Ref_Other}    ABC, 123 XYZ road
    click element    ${DE_Ref_AddressLine}
    input text    ${DE_Ref_AddressLine}    123,abc road.
    sleep    2s
    input text    ${DE_Ref_INS_InstituteName}    Vel Tech School
    sleep    1s
    Press Key    ${DE_Ref_INS_InstituteName}    \\13
    sleep    2s
    click element    ${DE_Ref_INS_Area}
    sleep    2s
    click element    ${DE_Ref_INS_Other}
    sleep    2s
    click element    ${DE_Ref_INS_OtherArea}
    input text    ${DE_Ref_INS_OtherArea}    xyz colony
    click element    ${DE_Ref_INS_AddressLine}
    input text    ${DE_Ref_INS_AddressLine}    CDE 123 near LMN
    click element    ${DE_Ref_INS_Landmark}
    input text    ${DE_Ref_INS_Landmark}    Near, ABC road
    sleep    2s
    click element    ${DE_Save}
    sleep    1s
    click element    ${Save_ok}

Reference Professional
    sleep    3s
    click element    ${DE_Ref_Reference}
    sleep    2s
    Reference Type Professional

Reference Type Professional
    click element    ${DE_Ref_RefType}
    sleep    2s
    click element    ${DE_Ref_Professional}
    click element    ${DE_Ref_Name}
    input text    ${DE_Ref_Name}    the king
    click element    ${DE_Ref_Designation}
    input text    ${DE_Ref_Designation}    Scrum Tester
    click element    ${DE_Ref_Relationship}
    input text    ${DE_Ref_Relationship}    Friend
    click element    ${DE_Ref_ContactNo}
    input text    ${DE_Ref_ContactNo}    1234567890
    click element    ${DE_Ref_Email}
    input text    ${DE_Ref_Email}    king@kingdom.com
    sleep    2s
    click element    ${DE_Ref_Pincode}
    input text    ${DE_Ref_Pincode}    606604
    sleep    1s
    Press Key    ${DE_Ref_Pincode}    \\13
    sleep    2s
    click element    ${DE_Ref_Area}
    click element    ${DE_Ref_OtherArea}
    click element    ${DE_Ref_Other}
    input text    ${DE_Ref_Other}    ABC, 123 XYZ road
    click element    ${DE_Ref_AddressLine}
    input text    ${DE_Ref_AddressLine}    123,abc road.
    sleep    2s
    input text    ${DE_Ref_INS_InstituteName}    Kadamba Technologies
    sleep    1s
    Press Key    ${DE_Ref_INS_InstituteName}    \\13
    sleep    2s
    click element    ${DE_Ref_INS_Area}
    sleep    2s
    click element    ${DE_Ref_INS_Other}
    sleep    2s
    #click element    ${DE_Ref_INS_OtherArea}
    #input text    ${DE_Ref_INS_OtherArea}    xyz colony
    click element    ${DE_Ref_INS_AddressLine}
    input text    ${DE_Ref_INS_AddressLine}    CDE 123 near LMN
    click element    ${DE_Ref_INS_Landmark}
    input text    ${DE_Ref_INS_Landmark}    Near, ABC road
    sleep    2s
    click element    ${DE_Save}
    sleep    1s
    click element    ${Save_ok}

Reference Employement
    sleep    3s
    click element    ${DE_Ref_Reference}
    sleep    2s
    Reference Type Employment

Reference Type Employment
    click element    ${DE_Ref_RefType}
    sleep    2s
    click element    ${DE_Ref_Employment}
    click element    ${DE_Ref_Name}
    input text    ${DE_Ref_Name}    the king
    click element    ${DE_Ref_Designation}
    input text    ${DE_Ref_Designation}    Scrum Tester
    click element    ${DE_Ref_Relationship}
    input text    ${DE_Ref_Relationship}    Friend
    click element    ${DE_Ref_ContactNo}
    input text    ${DE_Ref_ContactNo}    1234567890
    click element    ${DE_Ref_Email}
    input text    ${DE_Ref_Email}    king@kingdom.com
    sleep    2s
    click element    ${DE_Ref_Pincode}
    input text    ${DE_Ref_Pincode}    606604
    sleep    1s
    Press Key    ${DE_Ref_Pincode}    \\13
    sleep    2s
    click element    ${DE_Ref_Area}
    click element    ${DE_Ref_OtherArea}
    click element    ${DE_Ref_Other}
    input text    ${DE_Ref_Other}    ABC, 123 XYZ road
    click element    ${DE_Ref_AddressLine}
    input text    ${DE_Ref_AddressLine}    123,abc road.
    sleep    2s
    input text    ${DE_Ref_Name}    the king
    click element    ${DE_Ref_Designation}
    input text    ${DE_Ref_Designation}    Scrum Tester
    click element    ${DE_Ref_Relationship}
    click element    ${DE_Ref_HR_Name}
    input text    ${DE_Ref_HR_Name}    The King
    click element    ${DE_Ref_HR_Designation}
    input text    ${DE_Ref_HR_Designation}    Software tester
    click element    ${DE_Ref_HR_Contact}
    input text    ${DE_Ref_HR_Contact}    1234567890
    click element    xpath=(${DE_Ref_HR_Email})[2]
    input text    xpath=(${DE_Ref_HR_Email})[2]    king@kingdom.com
    input text    ${DE_Ref_INS_InstituteName}    Kadamba Technologies
    sleep    1s
    Press Key    ${DE_Ref_INS_InstituteName}    \\13
    sleep    2s
    click element    ${DE_Ref_INS_Area}
    sleep    2s
    click element    ${DE_Ref_INS_Other}
    sleep    2s
    #click element    ${DE_Ref_INS_OtherArea}
    #input text    ${DE_Ref_INS_OtherArea}    xyz colony
    click element    ${DE_Ref_INS_AddressLine}
    input text    ${DE_Ref_INS_AddressLine}    CDE 123 near LMN
    click element    ${DE_Ref_INS_Landmark}
    input text    ${DE_Ref_INS_Landmark}    Near, ABC road
    sleep    2s
    click element    ${DE_Save}
    sleep    1s
    click element    ${Save_ok}

Current Address
    sleep    3s
    click element    //span[text()='Data Entry']
    sleep    3s
    click element    ${DE_DB_Client}
    input text    ${DE_DB_Cli_Text}    Universal
    click element    ${DE_DB_Cli_Selc}
    input text    ${DE_Firstname_Location}    Lpgug
    input text    ${DE_Lastname_Location}    V
    click element    ${Search_Button}
    sleep    3s
    ${REF_NO}=    Get Text    ${DB_REFNO}
    Log To Console    ${REF_NO}
    click element    ${DB_REFNO}
    sleep    3s
    click element    ${DE_Address}
    Current Address Check

Current Address Check
    click element    ${DE_Add_FromDate}
    click element    ${DE_Add_DatePrev}
    click element    ${DE_Add_DateYear}
    click element    ${DE_Add_DatePrev}
    click element    ${DE_Add_DateYS}
    click element    ${DE_Add_DateMon}
    click element    ${DE_Add_Pincode}
    input text    ${DE_Add_Pincode}    606604
    sleep    1s
    Press Key    ${DE_Add_Pincode}    \\13
    sleep    2s
    click element    ${DE_Add_Area}
    click element    ${DE_Add_OtherArea}
    sleep    2s
    #Press Key    ${DE_Add_OtherArea}    \\13
    #sleep    5s
    #click element    ${DE_Add_Other}
    #input text    ${DE_Add_Other}    ABC 123 Road
    #sleep    2s
    click element    ${DE_Add_AddLine}
    input text    ${DE_Add_AddLine}    123, XYZ Road
    sleep    2s
    click element    ${DE_Add_LandMark}
    input text    ${DE_Add_LandMark}    near ABC XYZ road
    sleep    2s
    click element    ${DE_Add_IsRented}
    click element    ${DE_Add_IsRenOpt}
    click element    ${DE_Add_comments}
    input text    ${DE_Add_comments}    none
    sleep    2s
    click element    ${DE_Save}
    sleep    1s
    click element    ${Save_ok}
    sleep    3s

Permanent Address
    click element    ${DE_Add_Permanent}
    sleep    1s
    click element    ${DE_Add_SameAs}
    click element    ${DE_Add_SameAsOpt}
    sleep    2s
    click element    ${DE_Add_FromDate}
    click element    ${DE_Add_DatePrev}
    click element    ${DE_Add_DateYear}
    click element    ${DE_Add_DatePrev}
    click element    ${DE_Add_DateYS}
    click element    ${DE_Add_DateMon}
    click element    ${DE_Add_Pincode}
    sleep    2s
    click element    ${DE_Add_DateTill}
    sleep    2s
    click element    ${DE_Save}
    sleep    1s
    Element Should Contain    ${Alert_Message}    ${DE_Gap}
    Click element    ${Conformation_Alert_yes}
    click element    ${DE_Add_Gap}
    input text    ${DE_Add_Gap}    Please ensure the enter address date
    sleep    2s
    click element    ${DE_Add_ClearRp}

Education Data Entry For 10th and Highest 1
    ${Type_Status}=    Run Keyword and Return Status    Element Should Be Visible    ${DE_EDC_EduType}
    Run Keyword If    ${Type_Status} == True    Select from list by Label    ${DE_EDC_EduType}    10th
    click element    ${DE_Edc_InstituteName}
    input text    ${DE_Edc_InstituteName}    Vel Tech School
    Press Key    ${DE_Edc_InstituteName}    \\13
    click element    ${DE_Edc_NameOfCourse}
    input text    ${DE_Edc_NameOfCourse}    Higher Education
    click element    ${DE_Edc_MajorSubject}
    input text    ${DE_Edc_MajorSubject}    Maths, Computer Science
    sleep    2s
    Education Common Data Entry

Education Data Entry For 12th and Highest 2
    ${Type_Status}=    Run Keyword and Return Status    Element Should Be Visible    ${DE_EDC_EduType}
    Run Keyword If    ${Type_Status} == True    Select from list by Label    ${DE_EDC_EduType}    12th
    click element    ${DE_Edc_InstituteName}
    input text    ${DE_Edc_InstituteName}    Karnataka Secondary Education Examination Board
    Press Key    ${DE_Edc_InstituteName}    \\13
    click element    ${DE_Edc_NameOfCourse}
    input text    ${DE_Edc_NameOfCourse}    HSC
    click element    ${DE_Edc_MajorSubject}
    input text    ${DE_Edc_MajorSubject}    Computer Science
    sleep    2s
    Education Common Data Entry

Education Data Entry For UG1 and Highest 3
    ${Type_Status}=    Run Keyword and Return Status    Element Should Be Visible    ${DE_EDC_EduType}
    Run Keyword If    ${Type_Status} == True    Select from list by Label    ${DE_EDC_EduType}    UG 1
    click element    ${DE_Edc_InstituteName}
    input text    ${DE_Edc_InstituteName}    Sakthi Mariamman Engineering College
    Press Key    ${DE_Edc_InstituteName}    \\13
    click element    ${DE_Edc_NameOfCourse}
    input text    ${DE_Edc_NameOfCourse}    BE
    click element    ${DE_Edc_MajorSubject}
    input text    ${DE_Edc_MajorSubject}    CSC
    sleep    2s
    Education Common Data Entry

Education Data Entry For UG2 and Highest 4
    ${Type_Status}=    Run Keyword and Return Status    Element Should Be Visible    ${DE_EDC_EduType}
    Run Keyword If    ${Type_Status} == True    Select from list by Label    ${DE_EDC_EduType}    UG 2
    click element    ${DE_Edc_InstituteName}
    input text    ${DE_Edc_InstituteName}    Saveetha School of Engineering
    Press Key    ${DE_Edc_InstituteName}    \\13
    click element    ${DE_Edc_NameOfCourse}
    input text    ${DE_Edc_NameOfCourse}    BE
    click element    ${DE_Edc_MajorSubject}
    input text    ${DE_Edc_MajorSubject}    IT
    sleep    2s
    Education Common Data Entry

Education Data Entry For PG1 and Highest 5
    ${Type_Status}=    Run Keyword and Return Status    Element Should Be Visible    ${DE_EDC_EduType}
    Run Keyword If    ${Type_Status} == True    Select from list by Label    ${DE_EDC_EduType}    PG 1
    click element    ${DE_Edc_InstituteName}
    input text    ${DE_Edc_InstituteName}    Saveetha School of Engineering
    Press Key    ${DE_Edc_InstituteName}    \\13
    click element    ${DE_Edc_NameOfCourse}
    input text    ${DE_Edc_NameOfCourse}    ME
    click element    ${DE_Edc_MajorSubject}
    input text    ${DE_Edc_MajorSubject}    IT
    sleep    2s
    Education Common Data Entry

Education Data Entry For PG2 and Highest 6
    ${Type_Status}=    Run Keyword and Return Status    Element Should Be Visible    ${DE_EDC_EduType}
    Run Keyword If    ${Type_Status} == True    Select from list by Label    ${DE_EDC_EduType}    PG 2
    click element    ${DE_Edc_InstituteName}
    input text    ${DE_Edc_InstituteName}    Sakthi Mariamman Engineering College
    Press Key    ${DE_Edc_InstituteName}    \\13
    click element    ${DE_Edc_NameOfCourse}
    input text    ${DE_Edc_NameOfCourse}    MSC
    click element    ${DE_Edc_MajorSubject}
    input text    ${DE_Edc_MajorSubject}    CSC
    sleep    2s
    Education Common Data Entry

Education Common Data Entry
    click element    ${DE_Edc_TypeOfEducation}
    click element    ${DE_Edc_FullTime}
    click element    ${DE_Edc_CandidateName}
    input text    ${DE_Edc_CandidateName}    The King
    click element    ${DE_Edc_RegistrationNo}
    input text    ${DE_Edc_RegistrationNo}    512213205013
    click element    ${DE_Edc_CGPA}
    input text    ${DE_Edc_CGPA}    95%
    sleep    2s
    click element    ${DE_Edc_Coursestart}
    sleep    2s
    click element    ${Years}
    sleep    2s
    click element    ${Year}
    sleep    2s
    click element    ${Select_Month}
    sleep    2s
    click element    ${Select_Day}
    sleep    3s
    click element    ${DE_Edc_Courseend}
    sleep    2s
    click element    xpath=(//button[@class='btn btn-default btn-sm btn-block'])[2]
    click element    xpath=(//button[@ng-click='move(-1)'])[2]
    click element    //span[contains(text(),'May')]
    click element    xpath=(//span[text()='08'])[2]
    sleep    3s
    click element    ${DE_Edc_Gap}
    click element    ${DE_Edc_GapCmd}
    input text    ${DE_Edc_GapCmd}    Due to health issue 1 year i'm not go to school
    click element    ${DE_Edc_Comments}
    input text    ${DE_Edc_Comments}    none
    click element    ${DE_Save}
    Wait Until Element Is Visible    ${ok_path}
    Element Text Should Be    ${message_path}    Saved successfully
    Click Element    ${ok_path}
